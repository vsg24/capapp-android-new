package ir.capapp.mobile.adapter;

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.facebook.drawee.view.SimpleDraweeView
import ir.capapp.mobile.R
import ir.capapp.mobile.adapter.interfaces.OnItemClickListener
import ir.capapp.mobile.adapter.interfaces.OnLongItemClickListener
import ir.capapp.mobile.databinding.TinySourceCard
import ir.capapp.mobile.models.Source
import ir.capapp.mobile.viewmodel.SourceViewModel
import ir.capapp.mobile.models.Special
import ir.capapp.mobile.viewmodel.SpecialViewModel


public class TinyPriceListAdapter(private val context: Context?, public var list: ArrayList<Special>, private val recyclerView: RecyclerView) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val inflater = LayoutInflater.from(context);
    private var listener: ((View, Int) -> Unit)? = null;

    fun onItemClickListener(listener: (View, Int) -> Unit) {
        this.listener = listener;
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return TinyPriceViewHolder(TinySourceCard.inflate(inflater, parent, false));
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = list[position];
        val mPosition = holder.adapterPosition;

        (holder as TinyPriceViewHolder).bind(item, object : OnItemClickListener {
            override fun clicked(view: View, position: Int) {
                val itemClickedPosition = holder.adapterPosition;
                listener?.invoke(holder.itemView, itemClickedPosition);

            }
        }, null);
    }

    inner class TinyPriceViewHolder(private val binding: TinySourceCard) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Special?, listener: OnItemClickListener?, longClickListener: OnLongItemClickListener?) {
            val vm = SpecialViewModel();
            vm.special = item;
            val icon: SimpleDraweeView = binding.root.findViewById(R.id.icon);
            icon.clipToOutline = true;
            binding.viewModel = vm;
            binding.listener = listener;
            binding.longClickListener = longClickListener;
            binding.executePendingBindings();
        }
    }

}