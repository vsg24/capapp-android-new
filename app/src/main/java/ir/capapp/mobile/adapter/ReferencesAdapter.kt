package ir.capapp.mobile.adapter

import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import ir.capapp.mobile.R
import ir.capapp.mobile.models.References
import androidx.core.content.ContextCompat.startActivity
import android.content.Intent
import android.net.Uri


class ReferencesAdapter(val context: Context?, val list: ArrayList<References>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val inflater = LayoutInflater.from(context);

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MainViewHolder(inflater.inflate(R.layout.item_references, parent, false));
    }

    override fun getItemCount(): Int = list.size;

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {

        val item = list[position];
        if(holder is MainViewHolder) {
            holder.title.text = item.title;
            holder.title.paintFlags = holder.title.paintFlags or Paint.UNDERLINE_TEXT_FLAG;
            holder.title.setOnClickListener {
                if (context != null) {
                    val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(item.url))
                    startActivity(context, browserIntent, null);
                }
            }
        }
    }

    private inner class MainViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title: TextView = itemView.findViewById(R.id.references_title);
    }
}