package ir.capapp.mobile.adapter

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.button.MaterialButton
import com.google.gson.Gson
import ir.capapp.mobile.adapter.interfaces.OnItemClickListener
import ir.capapp.mobile.adapter.interfaces.OnLongItemClickListener
import ir.capapp.mobile.databinding.ItemSource
import ir.capapp.mobile.databinding.ItemSourceDisable
import ir.capapp.mobile.databinding.ItemSourcesHistory
import ir.capapp.mobile.databinding.ItemUserSource
import ir.capapp.mobile.models.Source
import ir.capapp.mobile.R
import ir.capapp.mobile.activity.SingleSourceActivity
import ir.capapp.mobile.viewmodel.SourceViewModel

public class SourceListAdapter(private val context: Context?, private val list: ArrayList<Source?>, private val history: Boolean = false, private var isUserCapitalList: Boolean = false) :
        RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var DISABLED_NORMAL_VIEW: Int = 0;
    private var MAIN_NORMAL_VIEW: Int = 1;
    private var BUTTONS_NORMAL_VIEW: Int = 2;
    private var MAIN_USER_SOURCE_VIEW: Int = 3;

    var expandedItem: Boolean = false;
    private val inflater = LayoutInflater.from(context);
    private var listener: ((View, Int) -> Unit)? = null;
    private var expandedItemPosition: Int = -1;
    private var charts: ArrayList<String> = ArrayList();

    fun onItemClickListener(listener: (View, Int) -> Unit) {
        this.listener = listener;
    }

    fun resetAdapter() {
        expandedItem = false;
        expandedItemPosition = -1;
    }

    override fun getItemCount(): Int {
        return list.size
    }

    override fun getItemViewType(position: Int): Int {
        val item = list[position];

        return if (item == null) {
            BUTTONS_NORMAL_VIEW;
        } else {
            if (isUserCapitalList) {
                MAIN_USER_SOURCE_VIEW;
            } else {
                if (!item.enabled) {
                    DISABLED_NORMAL_VIEW;
                } else {
                    MAIN_NORMAL_VIEW;
                }
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return if (history) {
            SourceHistoryViewHolder(ItemSourcesHistory.inflate(inflater, parent, false));
        } else {
            return if (viewType == MAIN_NORMAL_VIEW) {
                MainViewHolder(ItemSource.inflate(inflater, parent, false));
            } else if (viewType == DISABLED_NORMAL_VIEW) {
                DisableViewHolder(ItemSourceDisable.inflate(inflater, parent, false));
            } else if (viewType == MAIN_USER_SOURCE_VIEW) {
                UserSourceViewHolder(ItemUserSource.inflate(inflater, parent, false));
            } else {
                ButtonsViewHolder(inflater.inflate(R.layout.item_buttons, parent, false));
            }
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val item = list[position];
        val prevItem = if (position + 1 < list.size)
            list[position + 1];
        else
            null;

        val mPosition = holder.adapterPosition;

        when (holder) {
            is MainViewHolder -> holder.bind(item, object : OnItemClickListener {
                override fun clicked(view: View, position: Int) {
                    val itemClickedPosition = holder.adapterPosition;

                    expandedItemPosition = if (expandedItemPosition != itemClickedPosition) {
                        var itemRemoved = false;
                        if (expandedItemPosition != -1) {
                            list.removeAt(expandedItemPosition + 1);
                            notifyItemRemoved(expandedItemPosition + 1);

                            itemRemoved = true;
                        }

                        val realItemPosition = if (expandedItemPosition < itemClickedPosition && itemRemoved) itemClickedPosition - 1; else itemClickedPosition;

                        expandedItem = true;
                        list.add(realItemPosition + 1, null);
                        notifyItemInserted(realItemPosition + 1);

                        realItemPosition;
                    } else {
                        expandedItem = false;
                        list.removeAt(expandedItemPosition + 1);
                        notifyItemRemoved(expandedItemPosition + 1);
                        -1;
                    }

                    listener?.invoke(holder.itemView, itemClickedPosition);
                }
            }, null)
            is UserSourceViewHolder -> holder.bind(item, object : OnItemClickListener {
                override fun clicked(view: View, position: Int) {
                    val itemClickedPosition = holder.adapterPosition;
                    listener?.invoke(holder.itemView, itemClickedPosition);

//                    expandedItemPosition = if (expandedItemPosition != itemClickedPosition) {
//                        var itemRemoved = false;
//                        if (expandedItemPosition != -1) {
//                            list.removeAt(expandedItemPosition + 1);
//                            notifyItemRemoved(expandedItemPosition + 1);
//
//                            itemRemoved = true;
//                        }
//
//                        val realItemPosition = if (expandedItemPosition < itemClickedPosition && itemRemoved) itemClickedPosition - 1; else itemClickedPosition;
//
//                        list.add(realItemPosition + 1, null);
//                        notifyItemInserted(realItemPosition + 1);
//
//                        realItemPosition;
//                    } else {
//                        list.removeAt(expandedItemPosition + 1);
//                        notifyItemRemoved(expandedItemPosition + 1);
//                        -1;
//                    }
                }
            }, null)
            is DisableViewHolder -> holder.bind(item)
            is SourceHistoryViewHolder -> {
                item?.trending = when {
                    (prevItem?.value ?: 0.0) < (item?.value ?: 0.0) -> 1
                    (prevItem?.value ?: 0.0) > (item?.value ?: 0.0) -> -1
                    else -> 0
                }

                holder.bind(item, null, null);
            }
            is ButtonsViewHolder -> {
                holder.detailButton.setOnClickListener {
                    listener?.invoke(holder.detailButton, mPosition - 1);
                }
                holder.sourcePageButton.setOnClickListener {
                    listener?.invoke(holder.sourcePageButton, mPosition - 1);
                }
                holder.notification.setOnClickListener {
                    listener?.invoke(holder.notification, mPosition - 1);
                }
            }
        }
    }

    inner class DisableViewHolder(private val binding: ItemSourceDisable) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Source?) {
            val sourceViewModel: SourceViewModel? = SourceViewModel();
            sourceViewModel?.source = item;
            binding.viewModel = sourceViewModel;
            binding.executePendingBindings();
        }
    }

    inner class ButtonsViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val detailButton: MaterialButton = itemView.findViewById(R.id.item_buttons_source_detail);
        val sourcePageButton: MaterialButton = itemView.findViewById(R.id.item_buttons_source_source_bottom_sheet);
        val notification: MaterialButton = itemView.findViewById(R.id.item_buttons_notification);
    }

    inner class SourceHistoryViewHolder(private val binding: ItemSourcesHistory) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Source?, listener: OnItemClickListener?, longClickListener: OnLongItemClickListener?) {
            val sourceViewModel: SourceViewModel? = SourceViewModel();
            sourceViewModel?.source = item;
            binding.viewModel = sourceViewModel;
            binding.listener = listener;
            binding.longClickListener = longClickListener;
            binding.executePendingBindings();
        }
    }

    inner class MainViewHolder(private val binding: ItemSource) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Source?, listener: OnItemClickListener?, longClickListener: OnLongItemClickListener?) {
            val sourceViewModel: SourceViewModel? = SourceViewModel();
            sourceViewModel?.source = item;
            binding.viewModel = sourceViewModel;
            binding.listener = listener;
            binding.longClickListener = longClickListener;
            binding.executePendingBindings();
        }
    }

    inner class UserSourceViewHolder(private val binding: ItemUserSource) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Source?, listener: OnItemClickListener?, longClickListener: OnLongItemClickListener?) {
            val sourceViewModel: SourceViewModel? = SourceViewModel();
            sourceViewModel?.source = item;
            binding.viewModel = sourceViewModel;
            binding.listener = listener;
            binding.longClickListener = longClickListener;
            binding.executePendingBindings();
        }
    }

}