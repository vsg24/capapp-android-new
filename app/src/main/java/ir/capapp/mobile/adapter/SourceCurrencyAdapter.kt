package ir.capapp.mobile.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.BaseAdapter
import android.widget.ListAdapter
import android.widget.TextView
import ir.capapp.mobile.R
import ir.capapp.mobile.models.Source
import java.util.zip.Inflater

class SourceCurrencyAdapter(context: Context?, private val sources: ArrayList<Source>) : BaseAdapter() {

    private val inflater = LayoutInflater.from(context);

    override fun getView(p0: Int, p1: View?, p2: ViewGroup?): View {
        val v = inflater.inflate(R.layout.simple_list_item, p2, false);
        val title: TextView = v.findViewById(android.R.id.text1);
        val item = sources[p0];
        title.text = item.title;

        return v;
    }

    override fun getItem(p0: Int): Any = sources[p0];

    override fun getItemId(p0: Int): Long = 0;

    override fun getCount(): Int = sources.size;
}