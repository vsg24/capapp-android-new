package ir.capapp.mobile.adapter.interfaces

import android.view.View

interface OnItemClickListener {
    fun clicked(view: View, position: Int);
}