package ir.capapp.mobile.adapter.interfaces

import android.view.View

interface OnLongItemClickListener {
    fun longClicked(view: View, position: Int): Boolean;
}