package ir.capapp.mobile.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import ir.capapp.mobile.adapter.interfaces.OnItemClickListener
import ir.capapp.mobile.databinding.ItemSubscibeBinding
import ir.capapp.mobile.iab.SkuDetails
import ir.capapp.mobile.viewmodel.SKUViewModel

class InAppPurchasableProductListAdapter(val context: Context, val list: List<SkuDetails>) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val inflater = LayoutInflater.from(context);
    private var mListener: ((view: View, position: Int) -> Unit)? = null;

    fun setOnItemClickListener(listener: ((view: View, position: Int) -> Unit)?) {
        this.mListener = listener;
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return MainViewHolder(ItemSubscibeBinding.inflate(inflater, parent, false));
    }

    override fun getItemCount(): Int = list.size;

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val adapterPosition = holder.adapterPosition;
        val item = list[adapterPosition];
        if(holder is MainViewHolder) {
            holder.bind(item, object : OnItemClickListener {
                override fun clicked(view: View, position: Int) {
                    mListener?.invoke(view, adapterPosition);
                }
            });
        }
    }

    private inner class MainViewHolder(val binding: ItemSubscibeBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(inAppPurchasableProduct: SkuDetails, listener: OnItemClickListener) {
            val productViewModel = SKUViewModel();
            productViewModel.inAppPurchasableProduct = inAppPurchasableProduct;
            binding.viewModel = productViewModel;
            binding.listener = listener;
            binding.executePendingBindings();
        }
    }
}