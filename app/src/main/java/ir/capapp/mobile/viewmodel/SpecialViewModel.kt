package ir.capapp.mobile.viewmodel

import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.BindingAdapter
import com.facebook.drawee.view.SimpleDraweeView
import ir.capapp.mobile.BR
import ir.capapp.mobile.R
import ir.capapp.mobile.common.toReial
import ir.capapp.mobile.models.Special

class SpecialViewModel : BaseObservable() {
    private var mSpecial: Special? = Special();
    private var mTitle: String? = null;
    private var mValues: HashMap<String, Double>? = null;
    private var mImage: String? = null;
    private var mUnit: String? = null;

    var title: String?
        @Bindable
        get() {
            return mTitle;
        }
        set(value) {
            mTitle = value;
            notifyPropertyChanged(BR.title);
        }

    var values: HashMap<String, Double>?
        @Bindable
        get() {
            return mValues;
        }
        set(value) {
            mValues = value;
            notifyPropertyChanged(BR.values);
        }

    var unit: String?
        @Bindable
        get() {
            return mUnit;
        }
        set(value) {
            mUnit = value;
            notifyPropertyChanged(BR.unit);
        }

    var image: String?
        @Bindable
        get() {
            return mImage;
        }
        set(value) {
            mImage = value;
            notifyPropertyChanged(BR.image);
        }

    var special: Special?
        get() {
//            this.mSpecial?.favorite = favorite;
//            this.mSource?.chart = chart;
//            if (capital?.trim() != "")
//                this.mSource?.capital = capital?.toDouble();
//            else
//                this.mSource?.capital = 0.0;

            return mSpecial;
        }
        set(value) {
            mSpecial = value;
//            val d: String = if(value?.updatedAtTimestamp != null) {
//                val sdf = SimpleDateFormat("yyy/MM/dd", Locale.US);
//                val localTime = sdf.format(Date(value.updatedAtTimestamp * 1000));
//                JalaliCalendar().getJalaliDate(sdf.parse(localTime));
//            } else {
//                "unknown Date";
//            }

            title = mSpecial?.title;
            image = mSpecial?.image;
            unit = mSpecial?.unit;
            values = mSpecial?.values;
//            unit = mSource?.unit ?: Constant.Symbols.USD;
        }
}

@BindingAdapter("trending")
fun setTrendingImageUrl(imageView: ImageView, values: HashMap<String, Double>?) {
    try {
        if(values != null && values["current"] != null && values["yesterday"] != null) {
            if(values["current"]!! < values["yesterday"]!!) {
                // went down
                imageView.setImageResource(R.drawable.ic_trending_down);
                imageView.setColorFilter(ContextCompat.getColor(imageView.context, R.color.danger), android.graphics.PorterDuff.Mode.SRC_IN);
            } else if(values["current"]!! > values["yesterday"]!!) {
                // went up
                imageView.setImageResource(R.drawable.ic_trending_up);
                imageView.setColorFilter(ContextCompat.getColor(imageView.context, R.color.success), android.graphics.PorterDuff.Mode.SRC_IN);
            } else {
                // no change
                imageView.setImageResource(R.drawable.ic_trending_normal);
                imageView.setColorFilter(ContextCompat.getColor(imageView.context, R.color.darkGray), android.graphics.PorterDuff.Mode.SRC_IN);
            }
        }
    } catch (ignored: Exception) {}
}


@BindingAdapter("icon")
fun setIconUrl(imageView: SimpleDraweeView, url: String?) {
    if (url != null)
        imageView.setImageURI(url);

//    imageView.setro

//    imageView.hierarchy.setPlaceholderImage(R.drawable.source);
}

@BindingAdapter("format")
fun formatMoney(textView: TextView, vm: SpecialViewModel?) {
    try {
        textView.text = vm!!.values!!["current"]!!.toReial() + " " + vm.unit;
    } catch (ignored: Exception) {}
}
