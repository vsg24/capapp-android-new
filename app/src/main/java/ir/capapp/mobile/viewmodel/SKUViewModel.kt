package ir.capapp.mobile.viewmodel

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import ir.capapp.mobile.iab.SkuDetails

class SKUViewModel : BaseObservable() {

    private var mInAppPurchasableProduct: SkuDetails? = null;

    private var mPrice: String? = null;
    private var mDescription: String? = null;
    private var mTitle: String? = null;
    private var mType: String? = null;
    private var mSku: String? = null;

    public var inAppPurchasableProduct: SkuDetails?
        get() {
            return mInAppPurchasableProduct;
        }
        set(value) {
            mInAppPurchasableProduct = value;
            price = value?.price;
            title = value?.title;
            description = value?.description;
            type = value?.type;
            sku = value?.sku;
        }

    public var price: String?
        @Bindable
        get() {
            return mPrice;
        }
        set(value) {
            mPrice = value;
        }

    public var description: String?
        @Bindable
        get() {
            return mDescription;
        }
        set(value) {
            mDescription = value;
        }

    public var title: String?
        @Bindable
        get() {
            return mTitle;
        }
        set(value) {
            mTitle = value;
        }

    public var type: String?
        @Bindable
        get() {
            return mType;
        }
        set(value) {
            mType = value;
        }

    public var sku: String?
        @Bindable
        get() {
            return mSku;
        }
        set(value) {
            mSku = value;
        }
}