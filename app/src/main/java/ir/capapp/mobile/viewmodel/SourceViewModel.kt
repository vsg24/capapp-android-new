package ir.capapp.mobile.viewmodel

import android.util.Log
import android.view.View
import android.view.animation.AnimationUtils
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.BindingAdapter
import com.facebook.drawee.view.SimpleDraweeView
import ir.capapp.mobile.BR
import ir.capapp.mobile.R
import ir.capapp.mobile.common.NumberTextWatcher
import ir.capapp.mobile.common.Setting
import ir.capapp.mobile.constant.Constant
import ir.capapp.mobile.models.Source
import java.math.BigDecimal
import java.math.RoundingMode

class SourceViewModel : BaseObservable() {
    private var mShowChartIcon: Boolean = true;
    private var mSource: Source? = Source();
    private var mPercentage: Float? = null
    private var mTitle: String? = null
    private var mValue: Double? = null
    private var mUnit: String? = null
    private var mDate: String? = null
    private var mCapital: String? = null
    private var mTotalValue: BigDecimal? = null;
    private var mMessage: String? = null
    private var mFavorite: Boolean = false
    private var mChart: Boolean = false
    private var mTrending: Int = 0
    private var mImage: String? = null

    var chart: Boolean
        @Bindable
        get() {
            return mChart;
        }
        set(value) {
            mChart = value;
            notifyPropertyChanged(BR.chart);
        }

    var trending: Int
        @Bindable
        get() {
            return mTrending;
        }
        set(value) {
            mTrending = value;
            notifyPropertyChanged(BR.trending);
        }

    var percentage: Float?
        @Bindable
        get() {
            return mPercentage;
        }
        set(value) {
            mPercentage = value;
            notifyPropertyChanged(BR.percentage);
        }

    var title: String?
        @Bindable
        get() {
            return mTitle;
        }
        set(value) {
            mTitle = value;
            notifyPropertyChanged(BR.title);
        }

    var value: Double?
        @Bindable
        get() {
            return mValue;
        }
        set(value) {
            mValue = value;
            notifyPropertyChanged(BR.value);
        }

    var totalValue: BigDecimal?
        @Bindable
        get() {
            return mTotalValue;
        }
        set(value) {
            mTotalValue = value;
            notifyPropertyChanged(BR.totalValue);
        }

    var unit: String?
        @Bindable
        get() {
            return mUnit;
        }
        set(value) {
            mUnit = value;
            notifyPropertyChanged(BR.unit);
        }

    var showChartIcon: Boolean
        @Bindable
        get() {
            return mShowChartIcon;
        }
        set(value) {
            mShowChartIcon = value;
            notifyPropertyChanged(BR.showChartIcon);
        }

    var date: String?
        @Bindable
        get() {
            return mDate;
        }
        set(value) {
            mDate = value;
            notifyPropertyChanged(BR.date);
        }

    var capital: String?
        @Bindable
        get() {
            return mCapital;
        }
        set(value) {
            mCapital = value;
            notifyPropertyChanged(BR.capital);
        }

    var message: String?
        @Bindable
        get() {
            return mMessage;
        }
        set(value) {
            mMessage = value;
            notifyPropertyChanged(BR.message);
        }

    var favorite: Boolean
        @Bindable
        get() {
            return mFavorite;
        }
        set(value) {
            mFavorite = value;
            notifyPropertyChanged(BR.favorite);
        }

    var image: String?
        @Bindable
        get() {
            return mImage;
        }
        set(value) {
            mImage = value;
            notifyPropertyChanged(BR.image);
        }


    var source: Source?
        get() {
            this.mSource?.favorite = favorite;
            this.mSource?.chart = chart;
            if (capital?.trim() != "")
                this.mSource?.capital = capital?.toDouble();
            else
                this.mSource?.capital = 0.0;

            return mSource;
        }
        set(value) {
            mSource = value;
//            val d: String = if(value?.updatedAtTimestamp != null) {
//                val sdf = SimpleDateFormat("yyy/MM/dd", Locale.US);
//                val localTime = sdf.format(Date(value.updatedAtTimestamp * 1000));
//                JalaliCalendar().getJalaliDate(sdf.parse(localTime));
//            } else {
//                "unknown Date";
//            }

            title = mSource?.title;
            image = mSource?.image;
            message = mSource?.message
            date = mSource?.valueCreatedAt?.persianFormatted;
            this.value = mSource?.value?.toDouble() ?: 0.0;
            favorite = mSource?.favorite ?: false;
            chart = mSource?.chart ?: false;
            unit = mSource?.unit ?: Constant.Symbols.USD;
            trending = mSource?.trending ?: 0;
            percentage = mSource?.diffs?.yesterday?.percentage ?: 0f;

            val capital = mSource?.capital ?: 0 * 100000.0;
            var bCap = BigDecimal(capital).multiply(BigDecimal(100000000)).setScale(0, RoundingMode.HALF_UP).divide(BigDecimal(100000000)).toString();

            if (bCap.endsWith(".0")) {
                bCap = bCap.substring(0, bCap.length - 2);
            }
            this.capital = bCap;


            val tDec = BigDecimal(capital);
            this.totalValue = tDec.times(BigDecimal(value?.value ?: 0.0)).multiply(BigDecimal(100000000)).setScale(0, RoundingMode.HALF_UP).divide(BigDecimal(100000000));
        }

    fun onFavoriteButton(v: View) {
        if (!favorite) {
            val anim = AnimationUtils.loadAnimation(v.context, R.anim.like_button_animation);
            v.startAnimation(anim);
        }
        favorite = !favorite;
    }
}

@BindingAdapter("trending")
fun setTrendingImage(imageView: ImageView?, value: Int?) {
    try {
        if (imageView != null) {
            if (value == -1) {
                imageView.setImageResource(R.drawable.ic_trending_down);
                imageView.setColorFilter(ContextCompat.getColor(imageView.context, R.color.danger), android.graphics.PorterDuff.Mode.SRC_IN);
            } else if (value == 1) {
                imageView.setImageResource(R.drawable.ic_trending_up);
                imageView.setColorFilter(ContextCompat.getColor(imageView.context, R.color.success), android.graphics.PorterDuff.Mode.SRC_IN);
            }
        }
    }
    catch (ignored: Exception) {}
}

@BindingAdapter("image")
fun setImageUrl(imageView: SimpleDraweeView, url: String?) {
    if (url != null)
        imageView.setImageURI(url);

    val darkMode = Setting(imageView.context).darkMode;
    if (darkMode) {
        imageView.hierarchy.setPlaceholderImage(R.drawable.source_night);
    } else {
        imageView.hierarchy.setPlaceholderImage(R.drawable.source);
    }
}

@BindingAdapter("percentagecolor")
fun setPercentagecolor(view: TextView, percentage: Double) {
    if(percentage > 0.0) {
        view.setTextColor(ContextCompat.getColor(view.context, R.color.success));
    } else if(percentage < 0.0) {
        view.setTextColor(ContextCompat.getColor(view.context, R.color.danger));
    } else {
        view.setTextColor(ContextCompat.getColor(view.context, R.color.darkGray));
    }
}