package ir.capapp.mobile.viewmodel

import android.content.Intent
import android.widget.CompoundButton
import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import ir.capapp.mobile.BR
import ir.capapp.mobile.constant.Constant

class SettingViewModel : BaseObservable() {

    private var mDarkMode:Boolean? = false;
    private var mVersion: String? = null;
    private var mReferences: Boolean = false;

    var version: String?
        @Bindable
        get() {
            return mVersion;
        }
        set(value) {
            this.mVersion = value;
            notifyPropertyChanged(BR.version)
        }

    var references: Boolean
        @Bindable
        get() {
            return mReferences;
        }
        set(value) {
            this.mReferences = value;
            notifyPropertyChanged(BR.references);
        }

    var darkMode: Boolean
        @Bindable
    get() {
        return mDarkMode ?: false;
    }
    set(value) {
        this.mDarkMode = value;
        notifyPropertyChanged(BR.darkMode);
    }

    fun checkChange(c: CompoundButton, checked: Boolean) {
        val context = c.context;
        val i = Intent(Constant.Action.THEME);

        context.sendBroadcast(i);
    }
}