package ir.capapp.mobile.viewmodel;

//public class SourceViewModel extends BaseObservable {

//    private Source source;
//    private Float percentage;
//    private String title;
//    private Float value;
//    private String unit;
//    private Long date;
//    private String capital;
//    private String message;
//    private boolean favorite;
//    private String image;
//
//    public Source getSource() {
//        this.source.capital = getCapital().trim().equals("") || getCapital().trim().equals("0")? null : Float.valueOf(getCapital());
//        this.source.favorite = isFavorite();
//
//        return source;
//    }
//
//    public void setSource(Source source) {
//        this.source = source;
//
//            String capital = source.capital == null? "0" : Float.toString(source.capital);
//            String message = (source.message == null || source.message.trim().isEmpty())? null : source.message;
//
//            setTitle(source.title);
//            setImage(source.image);
//            setMessage(message);
//            setDate(source.updatedAtTimestamp);
//            setValue(source.value);
//            setFavorite(source.favorite);
//            setUnit(source.unit);
//            setPercentage(source.diffs.yesterday.getPercentage());
//            setCapital(capital);
//    }
//
//    @Bindable
//    public String getMessage() {
//        return message;
//    }
//
//    public void setMessage(String message) {
//        this.message = message;
//        notifyPropertyChanged(BR.message);
//    }
//
//    @Bindable
//    public String getUnit() {
//        return unit;
//    }
//
//    public void setUnit(String unit) {
//        this.unit = unit;
//        notifyPropertyChanged(BR.unit);
//    }
//
//    @Bindable
//    public Float getPercentage() {
//        return percentage;
//    }
//
//    public void setPercentage(Float percentage) {
//        this.percentage = percentage;
//        notifyPropertyChanged(BR.percentage);
//    }
//
//    @BindingAdapter("bind:image")
//    public static void setImageUrl(SimpleDraweeView imageView, String url) {
//        if (url != null)
//            imageView.setImageURI(Uri.parse(url));
//    }
//
//    public void onFavoriteButton(View v) {
//        if (!isFavorite()) {
//            Animation anim = AnimationUtils.loadAnimation(v.getContext(), R.anim.like_button_animation);
//            v.startAnimation(anim);
//        }
//        setFavorite(!isFavorite());
//    }
//
//    @Bindable
//    public Long getDate() {
//        return date;
//    }
//
//    public void setDate(Long date) {
//        this.date = date;
//        notifyPropertyChanged(BR.date);
//    }
//
//    @Bindable
//    public boolean isFavorite() {
//        return favorite;
//    }
//
//    public void setFavorite(boolean favorite) {
//        this.favorite = favorite;
//        notifyPropertyChanged(BR.favorite);
//    }
//
//    @Bindable
//    public String getCapital() {
//        return capital;
//    }
//
//    public void setCapital(String capital) {
//        this.capital = capital;
//        notifyPropertyChanged(BR.capital);
//    }
//
//    @Bindable
//    public String getImage() {
//        return image;
//    }
//
//    public void setImage(String image) {
//        this.image = image;
//        notifyPropertyChanged(BR.image);
//    }
//
//
//    @Bindable
//    public String getTitle() {
//        return title;
//    }
//
//    public void setTitle(String title) {
//        this.title = title;
//        notifyPropertyChanged(BR.title);
//    }
//
//    @Bindable
//    public Float getValue() {
//        return value;
//    }
//
//    public void setValue(Float value) {
//        this.value = value;
//        notifyPropertyChanged(BR.value);
//    }
//}
