package ir.capapp.mobile.constant

import ir.capapp.mobile.common.now

class Constant {

    companion object {
        val scale = 6;
    }

    class DataBase {
        companion object {
            val name = "CappAppDataBase";
        }
    }

    class Symbols {
        companion object {
            const val USD = "USD";
            const val IRT = "IRT";
            const val EUR = "EUR";
        }
    }

    class Action {
        companion object {
            const val SOURCE = "source_action";
            const val THEME = "theme_action";
        }
    }

    class Chart {
        companion object {
            const val LIMIT = 2;
        }
    }

    class Cache {
        companion object {
            val expiresOn: Long
            get() {
                return now + 5 * 60; //5 min for expirationDate
            }
        }
    }

    class NetWork {
        companion object {
            const val BASE_URL = "https://capapp.ir";
            const val API_VERSION = "/api/v1";
        }
    }
}
