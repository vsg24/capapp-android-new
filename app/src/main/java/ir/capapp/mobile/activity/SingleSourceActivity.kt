package ir.capapp.mobile.activity

import android.os.Bundle
import android.content.Context
import android.util.Log
import android.view.View
import android.view.WindowManager
import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.ContextCompat
import androidx.databinding.DataBindingComponent
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.cooltechworks.views.shimmer.ShimmerRecyclerView
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.ContentViewEvent
import com.crashlytics.android.answers.PurchaseEvent
import com.google.android.material.appbar.AppBarLayout
import com.google.gson.Gson
import io.supercharge.shimmerlayout.ShimmerLayout
import ir.capapp.mobile.R
import ir.capapp.mobile.adapter.SourceListAdapter
import ir.capapp.mobile.api.API
import ir.capapp.mobile.api.compare
import ir.capapp.mobile.api.history
import ir.capapp.mobile.common.*
import ir.capapp.mobile.models.Source
import ir.capapp.mobile.views.SourceLineChart

import kotlinx.android.synthetic.main.activity_single_source.*

class SingleSourceActivity : BaseActivity() {

    private var context: Context = this;
    private var list: ArrayList<Source?> = ArrayList();
    private var chartList: ArrayList<ArrayList<Source?>> = ArrayList();
    private var adapter: SourceListAdapter? = null;
    private var symbols: HashMap<String, Boolean> = HashMap();

    private lateinit var tabButton1: AppCompatButton;
    private lateinit var tabButton2: AppCompatButton;
    private lateinit var recyclerView: ShimmerRecyclerView;
    private lateinit var chart: SourceLineChart;
    private lateinit var chartShimmerLayout: ShimmerLayout;

    private var source: Source? = null;

    private var appBarLayout: AppBarLayout? = null;

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_single_source);
//        setSupportActionBar(single_source_toolbar);
        setDisplayHomeAsUpEnabled(supportActionBar);

        window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN;
        window.setFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS, WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
        // replace the line above with ones below to achieve complete transparency
//        setWindowFlag(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, false);
//        window.statusBarColor = Color.TRANSPARENT;

        source = Gson().fromJson(intent?.extras?.getString("source"), Source::class.java);

        if (source == null) {
            return;
        }

//        val includeCrypto = intent?.extras?.getBoolean("includeCrypto") ?: false;
        symbols[source!!.symbol] = source?.type == "crypto";

        recyclerView = single_source_list;
        chartShimmerLayout = single_source_chart_shimmer;
        chart = single_source_chart;
        tabButton1 = single_source_tab_m;
        tabButton2 = single_source_tab_h;

//        val scrollView: NestedScrollView = findViewById(R.id.scrollView);
//        appBarLayout = findViewById(R.id.app_bar);
//        scrollView.setOnScrollChangeListener(AppBarLayoutElevationScrollView(appBarLayout));

        chartShimmerLayout.startShimmerAnimation();

        initList();
        newRequest();

        backBtn.setOnClickListener {
            finish();
        }

        icon.hierarchy.setPlaceholderImage(R.drawable.ic_money);
        sourceTitle.text = source?.title;
        if (source?.symbol?.startsWith("X_", false) == false) {
            sourceSymbol.text = source?.symbol;
        }
        icon.setImageURI(source?.image);
        currentPrice.text = source?.value?.toReial() + " " + convertISOCurrencyUnitsForDisplay(source?.unit);
        currentPercentage.text = source?.diffs?.yesterday?.percentage.toString() + "%";

        if (source?.diffs?.yesterday?.percentage != null) {
            if (source?.diffs?.yesterday?.percentage!! < 0) {
                // went down
                trendingImage.setImageResource(R.drawable.ic_trending_down);
                trendingImage.setColorFilter(ContextCompat.getColor(context, R.color.danger), android.graphics.PorterDuff.Mode.SRC_IN);
                currentPercentage.setTextColor(ContextCompat.getColor(this, R.color.danger));
            } else if (source?.diffs?.yesterday?.percentage!! > 0) {
                trendingImage.setImageResource(R.drawable.ic_trending_up);
                trendingImage.setColorFilter(ContextCompat.getColor(context, R.color.success), android.graphics.PorterDuff.Mode.SRC_IN);
                currentPercentage.setTextColor(ContextCompat.getColor(this, R.color.success));
            } else {
                trendingImage.setImageResource(R.drawable.ic_trending_normal);
                trendingImage.setColorFilter(ContextCompat.getColor(context, R.color.darkGray), android.graphics.PorterDuff.Mode.SRC_IN);
                currentPercentage.setTextColor(ContextCompat.getColor(this, R.color.darkGray));
            }
        }

        if (source?.type == "crypto") {
            if (source?.symbol == "BTC") {
                backImage.setImageResource(R.drawable.bg_btc);
            } else {
                backImage.setImageResource(R.drawable.bg_crypto);
            }
        } else if (source?.symbol == "USD") {
            backImage.setImageResource(R.drawable.bg_usd);
        } else if (source?.symbol == "EUR") {
            backImage.setImageResource(R.drawable.bg_eur);
        } else if (source?.symbol == "GBP") {
            backImage.setImageResource(R.drawable.bg_gbp);
        } else if (source?.symbol == "AED") {
            backImage.setImageResource(R.drawable.bg_aed);
        } else if (source?.symbol == "TRY") {
            backImage.setImageResource(R.drawable.bg_try);
        } else if (source?.symbol == "JPY") {
            backImage.setImageResource(R.drawable.bg_jpy);
        } else if (source?.symbol == "QAR") {
            backImage.setImageResource(R.drawable.bg_qar);
        } else if (source?.symbol == "INR") {
            backImage.setImageResource(R.drawable.bg_inr);
        } else if (source?.symbol == "CAD") {
            backImage.setImageResource(R.drawable.bg_cad);
        } else if (source?.symbol == "RUB") {
            backImage.setImageResource(R.drawable.bg_rub);
        } else if (source?.symbol == "GEL") {
            backImage.setImageResource(R.drawable.bg_gel);
        } else if (source?.symbol == "IQD") {
            backImage.setImageResource(R.drawable.bg_iqd);
        } else if (source?.symbol == "CHF") {
            backImage.setImageResource(R.drawable.bg_chf);
        } else if (source?.symbol == "CNY") {
            backImage.setImageResource(R.drawable.bg_cny);
        } else if (source?.symbol == "X_GLD_24K_G" || source?.symbol == "X_GLD_18K_G" || source?.symbol == "X_GLD_OZ" || source?.symbol == "X_GLD_MESGHAL") {
            backImage.setImageResource(R.drawable.bg_gld);
        } else if (source?.symbol == "X_GLD_COIN_EMAMI" || source?.symbol == "X_GLD_COIN_QUARTER" || source?.symbol == "X_GLD_COIN_HALF" || source?.symbol == "X_GLD_COIN_BAHAR") {
            backImage.setImageResource(R.drawable.bg_gld_coin);
        }

        Answers.getInstance().logContentView(ContentViewEvent().putContentName(source?.title));
    }

    private fun setWindowFlag(bits: Int, on: Boolean) {
        val win = window;
        val winParams = win.attributes;
        if (on) {
            winParams.flags = winParams.flags or bits;
        } else {
            winParams.flags = winParams.flags and bits.inv();
        }
        win.attributes = winParams;
    }

    private fun newRequest() {
        getSources();
        getChartData();
    }

    fun tabSelected(v: View) {
        tabButton1.setTextColor(getThemeColor(context, R.attr.secondaryTextColor));
        tabButton2.setTextColor(getThemeColor(context, R.attr.secondaryTextColor));

        if (v is AppCompatButton) {
            val itemId = v.id;
            v.setTextColor(getThemeColor(context, R.attr.primaryTextColor));

            when (itemId) {
                tabButton1.id -> {
                    val compatibleSourceForChart = compatibleChartData(mapChartSources(chartList));
                    chart.resetChart();
                    chart.lineSourcesList.addAll(compatibleSourceForChart);
                    chart.notifyDataSetChange();
                }
                tabButton2.id -> {
                    val s = ArrayList<ArrayList<Source?>>();
                    s.add(list);
                    val compatibleSourceForChart = compatibleChartData(mapChartSources(s));

                    chart.resetChart();
                    chart.lineSourcesList.addAll(compatibleSourceForChart);
                    chart.notifyDataSetChange();
                }
            }
        }
    }

    private fun getSources() {
        API.Source.history(context, symbols) { sources, error ->
            if (sources != null) {
                if (sources.size > 0) {
                    val sourceHistory: ArrayList<Source> = sources[0];

                    try {
                        if (sourceHistory[0].bgImage != null) {
                            LoadImageFromUrl(backImage).execute(sourceHistory[0].bgImage);
                        }
                    } catch (e: Exception) {
                    }

                    list.addAll(sourceHistory);

                    adapter?.notifyDataSetChanged();
                    recyclerView.hideShimmerAdapter();
                    chartShimmerLayout.visibility = View.GONE;
                    chart.visibility = View.VISIBLE;
                }
            } else {

            }
        };
    }

    private fun getChartData() {
        API.Source.compare(context, symbols) { sources, error ->
            if (sources != null) {
                chartList = sources;
                val compatibleSource = compatibleChartData(mapChartSources(sources));
                chart.lineSourcesList.addAll(compatibleSource)
                chart.notifyDataSetChange();
            }
        };
    }

    private fun mapChartSources(sources: ArrayList<ArrayList<Source?>>?): HashMap<String, ArrayList<Source>> {
        val mapSources: HashMap<String, ArrayList<Source>> = HashMap();
        if (sources != null) {
            for (i in 0 until sources.size) {
                for (source in sources[i]) {
                    if (source == null)
                        continue;

                    val symbol = source.symbol;

                    if (symbols.keys.indexOfFirst { it == symbol } == -1)
                        continue;

                    if (mapSources.containsKey(symbol)) {
                        mapSources[symbol]?.add(source);
                    } else {
                        val sourceArray = arrayListOf(source);
                        mapSources[symbol] = sourceArray;
                    }
                }
            }
        }

        return mapSources;
    }


    private fun initList() {
        adapter = SourceListAdapter(context, list, true);
//        adapter?.onItemClickListener(onItemClickList);
        recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recyclerView.itemDecoration(context, R.drawable.inset_divider);
        //recyclerView.bouncyOverScroll();
        recyclerView.adapter = adapter;
        recyclerView.showShimmerAdapter();
        chartShimmerLayout.visibility = View.VISIBLE;
        chart.visibility = View.GONE;
    }
}
