package ir.capapp.mobile.activity

import android.content.Context
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.Gson
import com.scottyab.rootbeer.RootBeer
import io.github.inflationx.viewpump.ViewPumpContextWrapper
import ir.capapp.mobile.R
import ir.capapp.mobile.common.Setting
import ir.capapp.mobile.models.Source
import ir.capapp.mobile.repository.SourceRepository

open class BaseActivity : AppCompatActivity() {

    private var mContext: Context? = null;

    private var isDarkModeSelected: Boolean = false
        get() {
            return Setting(this).darkMode;
        }

//    private val mUserInventoryListener = IabHelper.QueryInventoryFinishedListener { result, inv ->
//        loadingProgressBar?.visibility = View.GONE;
//        if (result == null || result.isFailure) {
//            Toast.makeText(mContext, "لطفا کافه بازار را نصب و وارد حساب خود شوید.", Toast.LENGTH_LONG).show();
//            return@QueryInventoryFinishedListener;
//        }
//
//        inAppPurchasableProducts.clear();
//        for ((key, value) in inv.mSkuMap) {
//            inAppPurchasableProducts.add(value);
//        }
//
//        adapterInAppPurchasable?.notifyDataSetChanged();
//    }

    private fun detectRoot(): Boolean {
        val rb = RootBeer(mContext);
        val dangerousApps = arrayOf("com.kozhevin.rootchecks", "com.devadvance.rootcloak", "com.devadvance.rootcloak2");
        if (rb.checkForRootNative()
                || rb.detectPotentiallyDangerousApps(dangerousApps)
                || rb.detectRootCloakingApps()
                || rb.detectRootManagementApps()
                || rb.checkForSuBinary()
                || rb.checkForMagiskBinary()
                || rb.checkForRWPaths()
                || rb.checkSuExists()) {
            return true;
        }


        return false;
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);

        mContext = this;

        val a = SourceRepository().findAll();

        if(detectRoot()) {
//            finish();
//            Toast.makeText(this, "ROOT Detected", Toast.LENGTH_SHORT).show();
        }

        if (isDarkModeSelected) {
            setTheme(R.style.Dark)
        } else {
            setTheme(R.style.Light);
        }

        title = "";
        window.decorView.layoutDirection = LinearLayout.LAYOUT_DIRECTION_RTL;

        val darkMode = Setting(mContext).darkMode;

        val window = (mContext as AppCompatActivity).window;
        val view = window?.decorView;

        Handler().postDelayed({
            if (darkMode) {
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    var flags = view!!.systemUiVisibility;
                    flags = flags and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv();
                    view.systemUiVisibility = flags;
                }
            }
        }, 0);
    }

    override fun attachBaseContext(newBase: Context?) {
        super.attachBaseContext(ViewPumpContextWrapper.wrap(newBase!!));
    }

    protected fun setDisplayHomeAsUpEnabled(actionBar: ActionBar?) {
        actionBar?.setDisplayHomeAsUpEnabled(true);
        actionBar?.setHomeAsUpIndicator(R.drawable.ic_chevron_right);
    }

    override fun onSupportNavigateUp(): Boolean {
        finish();
        return super.onSupportNavigateUp()
    }
}
