package ir.capapp.mobile.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.gson.Gson
import ir.capapp.mobile.R
import ir.capapp.mobile.adapter.InAppPurchasableProductListAdapter
import ir.capapp.mobile.api.API
import ir.capapp.mobile.api.checkPurchase
import ir.capapp.mobile.api.getSKUWithUserSub
import ir.capapp.mobile.common.isPackageInstalled
import ir.capapp.mobile.common.now
import ir.capapp.mobile.iab.IabHelper
import ir.capapp.mobile.iab.IabResult
import ir.capapp.mobile.iab.SkuDetails
import ir.capapp.mobile.models.InAppPurchasableProductPurchaseResult
import kotlinx.android.synthetic.main.activity_subscription.*
import android.net.Uri
import android.view.LayoutInflater
import android.widget.*
import androidx.appcompat.app.AlertDialog
import co.ronash.pushe.Pushe
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.PurchaseEvent
import com.google.android.material.button.MaterialButton
import ir.capapp.mobile.common.copyToClipboard
import java.util.concurrent.TimeUnit


class SubscriptionActivity : BaseActivity() {

    lateinit var seekbar: SeekBar;
    lateinit var topIcon: ImageView;

    private var mContext = this;

    private var iabHelper: IabHelper? = null;
    private var inAppPurchasableProducts: ArrayList<SkuDetails> = ArrayList();
    private var adapterInAppPurchasable: InAppPurchasableProductListAdapter? = null;
    private var subscriptionText: TextView? = null;
    private var daysLeft: TextView? = null;
    private var loadingProgressBar: ProgressBar? = null;
    private var inAppPurchasableRecyclerView: RecyclerView? = null;
    private var purchaseEvent = PurchaseEvent();

    private var onItemClickListener: (View, Int) -> Unit = {
        _, position ->
            val item = inAppPurchasableProducts[position];
            iabHelper?.flagEndAsync();
            // get user purchased items
            iabHelper?.launchPurchaseFlow(mContext, item.sku, 0) { result, info ->
                purchaseEvent.putCustomAttribute("price", item.price)
                        .putItemId(item.sku)
                        .putItemName(item.title)
                        .putItemType(item.type)

                if (!result.isFailure) {
                    inAppPurchasableRecyclerView?.visibility = View.GONE;
                    loadingProgressBar?.visibility = View.VISIBLE;
                    val purchaseData = Gson().fromJson(info.originalJson, InAppPurchasableProductPurchaseResult::class.java);

                    API.Subscription.checkPurchase(mContext, purchaseData) { _result, _error ->
                        if(_result?.validUntilTimestampSec != null && _result.validUntilTimestampSec >= now) {

                            val dayLeft = TimeUnit.SECONDS.toDays((_result.validUntilTimestampSec - now));

                            inAppPurchasableRecyclerView?.visibility = View.GONE;
                            loadingProgressBar?.visibility = View.GONE;
                            subscriptionText?.visibility = View.VISIBLE;
                            daysLeft?.text = dayLeft.toString();
                            seekbar.progress =  calcPersonPosition(_result.initiationTimestampSec, _result.validUntilTimestampSec);
                            subscriptionText?.text = "تعداد روز باقی مانده از اشتراک شما:";

                            Toast.makeText(mContext, "موفقیت آمیز بود.", Toast.LENGTH_LONG).show();

                            purchaseEvent.putSuccess(true);
                        } else {
                            inAppPurchasableRecyclerView?.visibility = View.VISIBLE;
                            loadingProgressBar?.visibility = View.VISIBLE;
                            Toast.makeText(mContext, "موفقیت آمیز نبود.", Toast.LENGTH_LONG).show();

                            purchaseEvent.putSuccess(false);
                            purchaseEvent.putCustomAttribute("error", "bad response");
                        }
                    }
                } else {
                    purchaseEvent.putSuccess(false);
                    purchaseEvent.putCustomAttribute("error", result.message);

                    //Toast.makeText(mContext, result.message, Toast.LENGTH_LONG).show();
                }

                Answers.getInstance().logPurchase(purchaseEvent);
            };
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_subscription);
        setSupportActionBar(subscribe_toolbar)

        seekbar = dialog_subscribe_days_left_progress;
        seekbar.setOnTouchListener {
            _, _ ->
            true
        }

        val subscribe = intent?.extras?.getBoolean("subscribe", false);
        if (subscribe == null || !subscribe) {
            setDisplayHomeAsUpEnabled(supportActionBar);
        }

        loadingProgressBar = findViewById(R.id.loading);
        subscriptionText = findViewById(R.id.dialog_subscribe_text);
        daysLeft = findViewById(R.id.dialog_subscribe_days_left);
        topIcon = findViewById(R.id.topIcon);
        inAppPurchasableRecyclerView = findViewById(R.id.dialog_subscribe_list);

        topIcon.setOnClickListener {
            copyToClipboard(mContext, Pushe.getPusheId(applicationContext));
            Toast.makeText(mContext, "کد شناسایی ذخیره شد", Toast.LENGTH_SHORT).show();
        }

        subscribe();
    }

    private fun setupIABHelper(rsa: String?, listener: (IabResult) -> Unit) {
        iabHelper = IabHelper(mContext, rsa);
        iabHelper?.startSetup { result ->
            // Oh noes, there was a problem.
//            val activeSubs = iabHelper?.mService?.getPurchases(3, packageName, "subs", null);
//            val ownedSkus = activeSubs?.getStringArrayList("INAPP_PURCHASE_ITEM_LIST")
//            val purchaseDataList = activeSubs?.getStringArrayList("INAPP_PURCHASE_DATA_LIST")
//            val signatureList = activeSubs?.getStringArrayList("INAPP_DATA_SIGNATURE");

            listener.invoke(result);
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    public fun subscribe() {
        if (!isPackageInstalled("com.farsitel.bazaar", mContext.packageManager)) {
            loadingProgressBar?.visibility = View.GONE;
            val builder: AlertDialog.Builder = AlertDialog.Builder(mContext, R.style.AlertDialogCustom);
            builder.setTitle("مشکل در اجرای برنامه")
                    .setMessage("برای استفاده از برنامه باید کافه بازار را نصب کنید.")
                    .setPositiveButton("نصب کافه بازار") { dialog, which ->
                        startActivity(Intent(Intent.ACTION_VIEW, Uri.parse("https://cafebazaar.ir/download/bazaar.apk")));
                    }
                    .setNegativeButton("خروج") { dialog, which ->
                        finishAffinity();
                    }
                    .setCancelable(false)
                    .show();
            Toast.makeText(mContext, "لطفا کافه بازار را نصب کنید.", Toast.LENGTH_LONG).show();
            return;
        }

        adapterInAppPurchasable = InAppPurchasableProductListAdapter(this, inAppPurchasableProducts);
        adapterInAppPurchasable?.setOnItemClickListener(onItemClickListener);
        inAppPurchasableRecyclerView?.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false);
        inAppPurchasableRecyclerView?.adapter = adapterInAppPurchasable;

        inAppPurchasableRecyclerView?.visibility = View.GONE;
        loadingProgressBar?.visibility = View.VISIBLE;
        // _try to get a list of all available sku(s) from the server, also check if user is already subscribed
        API.Subscription.getSKUWithUserSub(mContext) { isSubscriptionValid, serverPurchaseResult, rsa, skuList, freeTrial ->
            if (isSubscriptionValid == null || !isSubscriptionValid) {
                subscriptionText?.text = "اشتراک فعالی یافت نشد.";

                if(freeTrial?.enabled == true) {
                    skuList?.add(freeTrial.sku);
                    initDialog(freeTrial.message);
                }

                inAppPurchasableRecyclerView?.visibility = View.VISIBLE;
                try {
                    setupIABHelper(rsa) { result ->
                        if (!result.isSuccess) {
                            return@setupIABHelper
                        }

                        iabHelper?.queryInventoryAsync(true, skuList, IabHelper.QueryInventoryFinishedListener { _result, inv ->
                            loadingProgressBar?.visibility = View.GONE;
                            if (_result == null || _result.isFailure) {
                                AlertDialog.Builder(mContext)
                                        .setTitle("خطا")
                                        .setMessage("لطفا کافه بازار را نصب و وارد حساب خود شوید.")
                                        .setNeutralButton("باشه", null)
                                        .show();
                                return@QueryInventoryFinishedListener;
                            }

                            inAppPurchasableProducts.clear();
                            for ((key, value) in inv.mSkuMap) {
                                inAppPurchasableProducts.add(value);
                            }
                            adapterInAppPurchasable?.notifyDataSetChanged();
                        });
                    };
                } catch (e: SecurityException) {
                    loadingProgressBar?.visibility = View.GONE;
                    val builder: AlertDialog.Builder = AlertDialog.Builder(mContext, R.style.AlertDialogCustom);
                    builder.setTitle("مشکل در اجرای برنامه")
                            .setMessage("لطفا ابتدا برنامه را حذف و سپس دوباره نصب کنید.")
                            .setNegativeButton("خروج") { dialog, which ->
                                finishAffinity();
                            }
                            .setCancelable(false)
                            .show();
                } catch (e: Exception) {
                    loadingProgressBar?.visibility = View.GONE;
                    Toast.makeText(mContext, "لطفا کافه بازار را اجرا کنید و وارد حساب خود شوید.", Toast.LENGTH_LONG).show();
                }
            } else {
                val dayLeft = TimeUnit.SECONDS.toDays((serverPurchaseResult?.validUntilTimestampSec?.minus(now)) ?: 0);

                inAppPurchasableRecyclerView?.visibility = View.GONE;
                loadingProgressBar?.visibility = View.GONE;
                subscriptionText?.text = "تعداد روز باقی مانده از اشتراک شما:";
                daysLeft?.text = dayLeft.toString();
                seekbar.progress = calcPersonPosition(serverPurchaseResult?.initiationTimestampSec, serverPurchaseResult?.validUntilTimestampSec);
            }
        };
    }

    private fun calcPersonPosition(start: Long?, end: Long?): Int {
        if(end != null && start != null) {
            val distance = end - start;
            val multiplier = 100.toFloat() / distance.toFloat();
            val offset = now - start;

            return (multiplier * offset).toInt();
        }

        return 0;
    }

    private fun initDialog(message: String) {
        val dialog = AlertDialog.Builder(mContext).create();
        val view: View = LayoutInflater.from(mContext).inflate(R.layout.dialog_gift, null, false);

        val button: MaterialButton = view.findViewById(R.id.dialog_gift_submit);
        val dialogMessage: TextView = view.findViewById(R.id.dialog_gift_text);
        dialogMessage.text = message;
        button.setOnClickListener {
            dialog.dismiss();
        }

        dialog.setView(view);
        dialog.show()
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data);

        // Pass on the activity result to the helper for handling
        if (iabHelper?.handleActivityResult(requestCode, resultCode, data) == false) {
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    public override fun onDestroy() {
        super.onDestroy();

        if (iabHelper != null) {
            iabHelper?.dispose();
        }

        iabHelper = null;
    }
}
