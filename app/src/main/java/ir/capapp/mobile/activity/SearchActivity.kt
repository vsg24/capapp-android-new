package ir.capapp.mobile.activity

import android.os.Bundle
import android.app.Activity
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Handler
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.crashlytics.android.answers.Answers
import com.crashlytics.android.answers.CustomEvent
import com.crashlytics.android.answers.SearchEvent
import com.google.android.material.button.MaterialButton
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ir.capapp.mobile.R
import ir.capapp.mobile.adapter.SourceListAdapter
import ir.capapp.mobile.api.API
import ir.capapp.mobile.api.priceChange
import ir.capapp.mobile.api.priceChangeDelete
import ir.capapp.mobile.api.utils.AppClient
import ir.capapp.mobile.common.SourceButtonsHandler
import ir.capapp.mobile.common.itemDecoration
import ir.capapp.mobile.constant.Constant
import ir.capapp.mobile.fragment.SourceBottomSheet
import ir.capapp.mobile.models.PriceChange
import ir.capapp.mobile.models.Source

import kotlinx.android.synthetic.main.activity_search.*

class SearchActivity : BaseActivity() {

    private val context: Context = this;
    private var adapter: SourceListAdapter? = null;
    private var list: ArrayList<Source?> = ArrayList();
    private var prices: ArrayList<PriceChange>? = null;
    private var temp: ArrayList<Source?>? = null;
    private var searchTimeOut: Handler = Handler();
    private var runnable: Runnable? = null;
    private var searchEvent = SearchEvent();
    private var sourceButtonsAction: SourceButtonsHandler? = null;

    private lateinit var searchEditText: EditText;
    private lateinit var recycler: RecyclerView;

    private val onTextChangeListener = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
            val text: String = p0.toString();

            try {
                searchTimeOut.removeCallbacks(runnable);
                runnable = Runnable {
                    if (text.length > 1) {
                        val sources = temp?.filter {
                            (if (it?.title?.toLowerCase()?.contains(text) != false)
                                it?.title?.toLowerCase()?.contains(text)
                            else
                                it.symbol?.toLowerCase()?.contains(text)
                                    )
                                    ?: false;
                        };
                        list.clear();
                        sources?.let { list.addAll(it) };
                        adapter?.notifyDataSetChanged();

                        searchEvent.putQuery(text);
                        Answers.getInstance().logSearch(searchEvent);
                    } else if (text.isEmpty()) {
                        list.clear();
                        list.addAll(temp ?: ArrayList());
                        adapter?.notifyDataSetChanged();
                    }
                }

                searchTimeOut.postDelayed(
                        runnable,
                        300);
            } catch (e: java.lang.Exception) {
                Answers.getInstance().logCustom(
                        CustomEvent("search")
                                .putCustomAttribute("error", e.message)
                );
            }
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }

        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {

        }
    }

    private val onItemClickList = { view: View, position: Int ->
        try {
            val id = view.id;

            if (sourceButtonsAction != null) {
                when (id) {
                    R.id.item_buttons_source_detail -> {
                        sourceButtonsAction?.detail(position);
                    }
                    R.id.item_buttons_source_source_bottom_sheet -> {
                        sourceButtonsAction?.bottomSheet(position, adapter, supportFragmentManager);
                    }
                    R.id.item_buttons_notification -> {
                        sourceButtonsAction?.notificationDialog(position) {

                        };
                    }
                }
            }
        } catch (ignored: Exception) {
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);

        searchEditText = search_edit_text;
        recycler = search_list;

        searchEditText.showSoftInputOnFocus = true;

        val tempJson = intent?.extras?.getString("sources", "");
        val priceJson = intent?.extras?.getString("prices", "");

        temp = Gson().fromJson(tempJson, object : TypeToken<ArrayList<Source>>() {}.type);
        prices = Gson().fromJson(priceJson, object : TypeToken<ArrayList<PriceChange>>() {}.type);

        sourceButtonsAction = SourceButtonsHandler(context, temp, prices);

        searchEditText.addTextChangedListener(onTextChangeListener);

        initList();
    }

    private fun initList() {
        adapter = SourceListAdapter(context, list);
        adapter?.onItemClickListener(onItemClickList);
        recycler.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recycler.itemDecoration(context, R.drawable.inset_right_divider);
        recycler.adapter = adapter;
    }
}
