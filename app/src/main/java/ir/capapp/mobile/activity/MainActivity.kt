package ir.capapp.mobile.activity;

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Typeface
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.bottomnavigation.BottomNavigationMenuView
import com.google.android.material.bottomnavigation.BottomNavigationView
import ir.capapp.mobile.R
import ir.capapp.mobile.api.API
import ir.capapp.mobile.api.get
import ir.capapp.mobile.application.App
import ir.capapp.mobile.constant.Constant
import ir.capapp.mobile.fragment.SettingFragment
import ir.capapp.mobile.fragment.SourcesFragment
import ir.capapp.mobile.fragment.SummaryFragment
import ir.capapp.mobile.models.PriceChange
import ir.capapp.mobile.models.Source
import ir.capapp.mobile.views.ClickToSelectEditText
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.android.synthetic.main.content_main.*
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ir.capapp.mobile.adapter.SourceCurrencyAdapter
import ir.capapp.mobile.api.checkForUpdate
import ir.capapp.mobile.api.utils.AppClient
import ir.capapp.mobile.common.*
import ir.capapp.mobile.fragment.ConvertFragment
import ir.capapp.mobile.models.References
import ir.capapp.mobile.models.Special
import java.lang.Exception
import java.math.BigDecimal
import java.math.RoundingMode

class MainActivity : BaseActivity() {
    // ok button on gift dialog does nothing when clicked
    // what is the point of subscriptionNote in activity_subscription?
    // add a help button at the top of app
    // search not working

    private lateinit var appBarLayout: AppBarLayout;

    private var menu: Menu? = null;
    private val settingReceiver = SettingReceiver();
    private val mContext: Context = this;
    private var sources: ArrayList<Source> = ArrayList();
    private val serverSetting = ServerSetting.getInstance();

    private val mOnNavigationItemSelectedListener = BottomNavigationView.OnNavigationItemSelectedListener { item ->
        when (item.itemId) {
            R.id.navigation_summary -> {
                appBarLayout.elevation = 0f;
                replaceFragment(getFragment(0));
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_sources -> {
                appBarLayout.elevation = 0f;
                replaceFragment(getFragment(1));
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_setting -> {
                appBarLayout.elevation = 0f;
                replaceFragment(getFragment(2));
                return@OnNavigationItemSelectedListener true
            }
            R.id.navigation_convert -> {
                appBarLayout.elevation = 0f;
                replaceFragment(getFragment(3));
                return@OnNavigationItemSelectedListener true
            }
        }
        false
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setSupportActionBar(toolbar);
        registerReceiver(settingReceiver, IntentFilter(Constant.Action.THEME));

        appBarLayout = findViewById(R.id.main_app_bar);
        main_navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener);
        if (!App.isRecreated)
            main_navigation.selectedItemId = R.id.navigation_summary;
        App.isRecreated = false;

        applyBottomNavFont();
        checkForUpdate();
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        this.menu = menu;
        menuInflater.inflate(R.menu.main_menu, menu);

        if (!serverSetting.conversionEnable) {
            menu?.findItem(R.id.convert)?.isVisible = false;
        }


        return super.onCreateOptionsMenu(menu);
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        val id = item?.itemId;

        when (id) {
            R.id.account -> {
                val intent = Intent(mContext, SubscriptionActivity::class.java);
                startActivity(intent);
            }
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onDestroy() {
        super.onDestroy();
        unregisterReceiver(settingReceiver);
    }

    fun openSourceFragment() {
        main_navigation.selectedItemId = R.id.navigation_sources;
    }

    private fun getSourcesArrayList(): ArrayList<String> {
        val l = ArrayList<String>();
        for (s in sources) {
//            if (s.type == "crypto") {
//                continue;
//            }
            l.add(s.title);
        }

        return l;
    }

    private fun getSourceByTitle(title: String): Source? {
        for (s in sources) {
            if (s.title == title) {
                return s;
            }
        }

        return null;
    }

    private fun checkForUpdate() {
        val dialog = AlertDialog.Builder(mContext).create();
        val view = LayoutInflater.from(mContext).inflate(R.layout.dialog_update, null, false);
        val downloadButton: MaterialButton = view.findViewById(R.id.dialog_update_submit);
        val cancelButton: MaterialButton = view.findViewById(R.id.dialog_update_cancel);

        var l = "";
        var pl = false;

        cancelButton.setOnClickListener {
            dialog.dismiss();
        }

        downloadButton.setOnClickListener {
            if (!pl) {
                if (isPackageExist(mContext, "com.farsitel.bazaar")) {
                    intent = Intent(Intent.ACTION_VIEW);
                    intent.data = Uri.parse("bazaar://details?id=$packageName");
                    intent.setPackage("com.farsitel.bazaar");
                    startActivity(intent);

                    return@setOnClickListener;
                }
            }
            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(l));
            startActivity(browserIntent);
        }

        dialog.setView(view);

        API.Main.checkForUpdate(mContext) { updateAvailable, newVersionCode, forcedUpdate, preferLink, link ->
            if (updateAvailable == true) {
                l = link ?: "";
                pl = preferLink ?: false
                if (forcedUpdate == true) {
                    cancelButton.visibility = View.GONE;
                    dialog.setCancelable(false);
                    AppClient.getInstance(this).clearRequestQueue();
                }
                dialog.show();
            }
        }
    }

    fun getSource(listener: (ArrayList<Source>?, ArrayList<Special>?, ArrayList<PriceChange>?, ArrayList<References>?, String?) -> Unit) {
        API.Source.get(mContext, true) { response, specials, prices, references, conversationEnable, error ->
            listener(response, specials, prices, references, error);
            if (response != null) {
                sources.clear();
                serverSetting.references.clear();

                serverSetting.references.addAll(references ?: ArrayList());
                sources.addAll(response);

                serverSetting.conversionEnable = conversationEnable ?: true;
                invalidateOptionsMenu();
            }
        }
    }

    private fun getFragment(index: Int): Fragment {
        return when (index) {
            0 -> SummaryFragment();
            1 -> SourcesFragment();
            2 -> SettingFragment();
            3 -> ConvertFragment();
            else -> TODO();
        }
    }

    private fun replaceFragment(fragment: Fragment) {
        val bundle = Bundle();
        bundle.putString("SOURCE", Gson().toJson(sources));
        fragment.arguments = bundle;
        supportFragmentManager.beginTransaction().replace(R.id.main_frame, fragment).commit();
    }

    private fun applyBottomNavFont() {
        val font = Typeface.createFromAsset(this.assets, App.defaultFontPath); //replace it with your own font
        for (i in 0 until main_navigation.childCount) {
            val child = main_navigation.getChildAt(i)
            if (child is BottomNavigationMenuView) {
                for (j in 0 until child.childCount) {
                    val item: View = child.getChildAt(j)
                    val smallItemText: TextView? = item.findViewById(R.id.smallLabel)
                    if (smallItemText is TextView) {
                        smallItemText.typeface = font;
                    }
                    val largeItemText: TextView? = item.findViewById(R.id.largeLabel)
                    if (largeItemText is TextView) {
                        largeItemText.typeface = font;
                    }
                }
            }
        }
    }

    inner class SettingReceiver : BroadcastReceiver() {
        override fun onReceive(p0: Context?, p1: Intent?) {
            val darkMode = Setting(mContext).darkMode;

            Setting(mContext).darkMode = !darkMode;
            replaceFragment(getFragment(2));
            App.isRecreated = true;
            recreate(600);
        }
    }
}
