package ir.capapp.mobile.application

import android.app.Application
import co.ronash.pushe.Pushe
import com.facebook.drawee.backends.pipeline.Fresco
import com.facebook.stetho.Stetho
import ir.capapp.mobile.constant.Constant
import io.github.inflationx.calligraphy3.CalligraphyConfig
import io.github.inflationx.calligraphy3.CalligraphyInterceptor
import io.github.inflationx.viewpump.ViewPump
import android.graphics.Color
import com.crashlytics.android.Crashlytics
import com.crashlytics.android.answers.Answers
import io.fabric.sdk.android.Fabric

import ir.capapp.mobile.R
import ir.capapp.mobile.common.OpenHelper
import ir.capapp.mobile.models.DaoMaster
import ir.capapp.mobile.models.DaoSession

class App : Application() {
    companion object {
        var daoSession: DaoSession? = null;
        var defaultFontPath = "fonts/Shabnam-Light.ttf";
        var isRecreated: Boolean = false;
    }

    override fun onCreate() {
        super.onCreate();

        try {
            Fresco.initialize(this);
        } catch (e: Exception) { }

        try {
            Fabric.with(this, Crashlytics(), Answers());
        } catch (ignored: Exception) {}

        try {
            Pushe.initialize(this, true);

            Pushe.createNotificationChannel(
                    this,
                    "price_alert",
                    "هشدار قیمت",
                    "هشدار های قیمت که در برنامه توسط شما تنظیم شدند.",
                    5, //importance: Number between 0 to 5 (5 is the most important)
                    true,
                    true,
                    true, //showBadge
                    Color.YELLOW, // led color
                    null);

            Pushe.createNotificationChannel(
                    this,
                    "update",
                    "به روز رسانی",
                    "به روز رسانی CAP",
                    4, //importance: Number between 0 to 5 (5 is the most important)
                    true, //enableLight
                    false, //enableViberation
                    true, //showBadge
                    Color.WHITE, // led color
                    null);
        } catch (ignored: Exception) {}

        val helper = OpenHelper(this, Constant.DataBase.name);
        val db = helper.writableDatabase;
        val daoMaster = DaoMaster(db);
        val daoSession = daoMaster.newSession();
        App.daoSession = daoSession;

        Stetho.initializeWithDefaults(this);

        ViewPump.init(ViewPump.builder()
                .addInterceptor(CalligraphyInterceptor(
                        CalligraphyConfig.Builder()
                                .setDefaultFontPath(App.defaultFontPath)
                                .setFontAttrId(R.attr.fontPath)
                                .build()))
                .build())
    }
}