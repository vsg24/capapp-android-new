package ir.capapp.mobile.common

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ir.capapp.mobile.constant.Constant

class UserManager(val context: Context?) {
    var symbols: HashMap<String, Boolean>?
    get() {
        return try {
            var charts: HashMap<String, Boolean>? = Gson().fromJson(SharedPreferencesHelper(context).getString("CHARTS", ""), object : TypeToken< HashMap<String, Boolean>>(){}.type);

            if(charts?.size == 0) {
                charts = null;
            }
            return charts;
        } catch (e: Exception) {
            null;
        }
    }
    set(value) {
        SharedPreferencesHelper(context).save("CHARTS", Gson().toJson(value));
    }
}