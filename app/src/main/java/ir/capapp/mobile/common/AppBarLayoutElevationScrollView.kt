package ir.capapp.mobile.common

import android.animation.ValueAnimator
import androidx.core.widget.NestedScrollView
import com.google.android.material.appbar.AppBarLayout

class AppBarLayoutElevationScrollView(private val appBarLayout: AppBarLayout?) : NestedScrollView.OnScrollChangeListener {
    override fun onScrollChange(v: NestedScrollView?, scrollX: Int, scrollY: Int, oldScrollX: Int, oldScrollY: Int) {
        if(v?.canScrollVertically(-1) == true) {
            appBarLayout?.elevation = 10f;
        } else {
            appBarLayout?.elevation = 0f;
        }
    }
}