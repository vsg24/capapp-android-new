package ir.capapp.mobile.common

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.TextView
import android.widget.Toast
import androidx.fragment.app.FragmentManager
import com.google.android.material.button.MaterialButton
import com.google.gson.Gson
import ir.capapp.mobile.R
import ir.capapp.mobile.activity.SingleSourceActivity
import ir.capapp.mobile.adapter.SourceListAdapter
import ir.capapp.mobile.api.API
import ir.capapp.mobile.api.priceChange
import ir.capapp.mobile.api.priceChangeDelete
import ir.capapp.mobile.api.utils.AppClient
import ir.capapp.mobile.constant.Constant
import ir.capapp.mobile.fragment.SourceBottomSheet
import ir.capapp.mobile.models.PriceChange
import ir.capapp.mobile.models.Source
import java.math.BigDecimal
import java.math.RoundingMode

class SourceButtonsHandler(private val context: Context?, private val list: ArrayList<Source?>?, private val prices: ArrayList<PriceChange>?) {

    fun detail(position: Int) {
        val item = list?.get(position);

        val intent = Intent(context, SingleSourceActivity::class.java);
        intent.putExtra("source", Gson().toJson(item));
        context?.startActivity(intent);
    }

    fun bottomSheet(position: Int, adapter: SourceListAdapter?, fm: FragmentManager) {
        val item = list?.get(position);
        val bundle = Bundle();
        bundle.putString(Constant.Action.SOURCE, Gson().toJson(item));
        val sourceBottomSheet = SourceBottomSheet();
        sourceBottomSheet.setOnDialogDismissListener { s ->
            updateItem(s, adapter);
        }
        sourceBottomSheet.arguments = bundle;
        sourceBottomSheet.show(fm, "tag");
    }

    fun notificationDialog(position: Int, listener: () -> Unit) {
        val source = list?.get(position);

        val dialog = AlertDialog.Builder(context).create();
        val view = LayoutInflater.from(context).inflate(R.layout.dialog_change_price, null, false);
        dialog.setView(view);

        val button: MaterialButton = view.findViewById(R.id.dialog_change_price_submit);
        val loading: ProgressBar = view.findViewById(R.id.dialog_change_price_loading);
        val deleteButton: MaterialButton = view.findViewById(R.id.dialog_change_price_delete);
        val input: EditText = view.findViewById(R.id.dialog_change_price_value);

        input.setSelectAllOnFocus(true);
        input.addTextChangedListener(NumberTextWatcher(input));

        val indexOfPrice = prices?.indexOfFirst { it.symbol == source?.symbol };
        val priceChange: PriceChange = if (indexOfPrice != -1) {
            deleteButton.visibility = View.VISIBLE;
            val p = PriceChange();
            p.symbol = source?.symbol;
            p.unit = source?.unit;

            val value = prices?.get(indexOfPrice ?: 0)?.threshold ?: 0.0;
            var bCap = BigDecimal(value).multiply(BigDecimal(100000000)).setScale(0, RoundingMode.HALF_UP).divide(BigDecimal(100000000)).toString();
            if (bCap.endsWith(".0")) {
                bCap = bCap.substring(0, bCap.length - 2);
            }

            input.setText(bCap);
            p;
        } else {
            val p = PriceChange();
            p.symbol = source?.symbol;
            p.unit = source?.unit;
            p;
        }

        deleteButton.setOnClickListener {
            deleteButton.visibility = View.GONE;
            button.visibility = View.GONE;
            loading.visibility = View.VISIBLE;

            API.Subscription.Notification.priceChangeDelete(context, source?.symbol) { error ->
                if (error != null) {
                    deleteButton.visibility = View.VISIBLE;
                    button.visibility = View.VISIBLE;
                    loading.visibility = View.GONE;

                    Toast.makeText(context, "خطای غیر منتظره دوباره تلاش کنید.", Toast.LENGTH_SHORT).show();
                } else {
                    listener.invoke();
                    dialog.dismiss();
                }
            }
        }

        button.setOnClickListener {
            try {
                deleteButton.visibility = View.GONE;
                button.visibility = View.GONE;
                loading.visibility = View.VISIBLE;

                priceChange.threshold = input.text.toString().replace(",", "").toDouble();

                API.Subscription.Notification.priceChange(context, priceChange) { error ->
                    if (error == null) {
                        listener.invoke();
                        dialog.dismiss();
                    } else {
                        deleteButton.visibility = View.VISIBLE;
                        button.visibility = View.VISIBLE;
                        loading.visibility = View.GONE;

                        dialog.dismiss();
                    }
                }
            } catch (e: java.lang.Exception) {
                e.printStackTrace();
                button.visibility = View.VISIBLE;
                loading.visibility = View.GONE;
            }
        }

        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
        dialog.show();
        AppClient.getInstance(context).clearRequestQueue();
    }

    private fun updateItem(s: Source?, adapter: SourceListAdapter?) {
        try {
            val index = list?.indexOfFirst { it?.symbol == s?.symbol };
            val source = list?.get(index ?: 0);
            source?.favorite = s?.favorite ?: false;
            source?.chart = s?.chart ?: false;
            source?.capital = s?.capital;
            adapter?.notifyDataSetChanged();
        } catch (e: Exception) {
            e.printStackTrace();
        }
    }
}