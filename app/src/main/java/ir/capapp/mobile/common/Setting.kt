package ir.capapp.mobile.common

import android.content.Context
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class Setting(val context: Context?) {
    var order: HashMap<String, Int>
        get() {
            val value = SharedPreferencesHelper(context).getString("ORDER", "[]");
            return Gson().fromJson(value, object : TypeToken<HashMap<String, Int>>() {}.type)
        }
        set(value) {
            SharedPreferencesHelper(context).save("ORDER", Gson().toJson(value));
        }

    var darkMode: Boolean
    get() {
        return SharedPreferencesHelper(context).getBoolean("DARKMODE", false);
    }
    set(value) {
        SharedPreferencesHelper(context).save("DARKMODE", value);
    }
}