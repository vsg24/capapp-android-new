package ir.capapp.mobile.common

import ir.capapp.mobile.constant.Constant
import ir.capapp.mobile.models.References

class ServerSetting  {

    var references: ArrayList<References> = ArrayList();
    var conversionEnable: Boolean = false;
    var fallbackSourceChart: String = Constant.Symbols.EUR;

    companion object {
        private var INSTANCE: ServerSetting? = null;
        fun getInstance(): ServerSetting =
                INSTANCE ?: synchronized(this) {
                INSTANCE ?: ServerSetting().also {
                    INSTANCE = it;
                }
            }
    }
}