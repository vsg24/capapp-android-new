package ir.capapp.mobile.common

import android.animation.ValueAnimator
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.appbar.AppBarLayout

class AppBarLayoutElevationRecycler(private val appbar: AppBarLayout?) : RecyclerView.OnScrollListener() {
    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy);
        if(recyclerView.canScrollVertically(-1)) {
            appbar?.elevation = 10f;
        } else {
            appbar?.elevation = 0f;
        }
    }
}
