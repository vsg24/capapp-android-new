package ir.capapp.mobile.common

import android.content.Context
import android.net.Uri
import android.os.Build
import android.os.CountDownTimer
import android.view.View
import android.webkit.URLUtil
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.appbar.AppBarLayout
import ir.capapp.mobile.R
import ir.capapp.mobile.constant.Constant
import me.everything.android.ui.overscroll.OverScrollDecoratorHelper
import java.lang.Exception
import java.text.NumberFormat
import java.util.*
import android.R.attr.data
import android.content.ClipData
import android.content.ClipboardManager
import androidx.annotation.ColorInt
import android.content.res.Resources.Theme
import android.util.TypedValue
import android.content.pm.PackageManager
import android.graphics.drawable.Drawable
import android.util.DisplayMetrics
import android.view.inputmethod.InputMethodManager
import android.widget.ScrollView
import androidx.core.view.ViewCompat
import androidx.core.widget.NestedScrollView
import co.ronash.pushe.Pushe
import ir.capapp.mobile.activity.BaseActivity
import ir.capapp.mobile.models.Source
import java.math.BigDecimal
import java.math.RoundingMode
import java.text.DecimalFormat

val now: Long
    get() {
        return System.currentTimeMillis() / 1000;
    }

fun isPackageExist(context: Context, pkgName: String): Boolean {
    val packageManager = context.packageManager;
    try {
        val info = packageManager.getPackageInfo(pkgName, PackageManager.GET_META_DATA);
    } catch (e: Exception) {
        return false;
    }
    return true;
}

fun AppCompatActivity.recreate(delay: Long) {
    object : CountDownTimer(delay, delay) {
        override fun onFinish() {
            recreate();
        }

        override fun onTick(p0: Long) {
        }

    }.start();
}

fun URLUtil.urlMaker(url: String, query: String = ""): String {

    val completeUrl: ArrayList<String> = ArrayList();

    val domain = Constant.NetWork.BASE_URL + Constant.NetWork.API_VERSION;
    completeUrl.add(domain);
    completeUrl.add(if (url.toCharArray()[0].toString() == "/") url else "/$url");

    var uri: String = completeUrl.joinToString("");

    if (query != "") {
        val buildUpon = Uri.parse(uri)
                .buildUpon();

        val queryString: List<String> = query.split("&");
        for (q in queryString) {
            val key = q.split("=");
            if (key.size > 1 && key[1] != "null")
                buildUpon.appendQueryParameter(key[0], key[1]);
        }
        uri = buildUpon.build().toString();
    }

    return uri;
}

fun RecyclerView.itemDecoration(context: Context?, id: Int) {
    val itemDecoration = DividerItemDecoration(context, DividerItemDecoration.VERTICAL);
    itemDecoration.setDrawable(ContextCompat.getDrawable(context!!, id)!!);
    this.addItemDecoration(itemDecoration);
}

fun RecyclerView.liftOnScroll(appBar: AppBarLayout?) {
    this.addOnScrollListener(AppBarLayoutElevationRecycler(appBar));
}

fun RecyclerView.bouncyOverScroll() {
    OverScrollDecoratorHelper.setUpOverScroll(this, OverScrollDecoratorHelper.ORIENTATION_VERTICAL);
}

fun NestedScrollView.bouncyOverScroll() {
//    OverScrollDecoratorHelper.setUpOverScroll(this as ScrollView);
}


fun NestedScrollView.liftOnScroll(appBar: AppBarLayout?) {
    this.setOnScrollChangeListener(AppBarLayoutElevationScrollView(appBar));
}

fun hideSoftInput(context: Context?, view: View?) {
    val imm: InputMethodManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager;
    imm.hideSoftInputFromWindow(view?.windowToken, 0);
}

fun showSoftInput(context: Context?, view: View?) {
    val imm: InputMethodManager = context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager;
    imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
}

//fun Long.toReial(): String {
//    val format = DecimalFormat("#,###");
//
//    return format.format(this);
//}

//fun Float.toReial(): String {
//    val format = DecimalFormat("#,###");
//
//    return format.format(this);
//}

fun lightStatusBar(context: Context?, enable: Boolean) {
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        val window = (context as AppCompatActivity).window;
        val view = window?.decorView;

        var flags = view?.systemUiVisibility;
        if (flags != null) {
            flags = if (enable) {
                flags or View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
            } else {
                flags and View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR.inv();
            }
            view?.systemUiVisibility = flags
        }
    }
}

fun getThemeColor(context: Context?, source: Int): Int {
    val typedValue = TypedValue();
    val theme = context?.theme;
    theme?.resolveAttribute(source, typedValue, true);
    @ColorInt val color = typedValue.data;

    return color;
}

fun convertISOCurrencyUnitsForDisplay(unit: String?): String? {
    return try {
        val iSOUnits: HashMap<String, String> = HashMap();
        iSOUnits["IRT"] = "تومان";
        iSOUnits["USD"] = "دلار";
        iSOUnits["EUR"] = "یورو";

        return if (iSOUnits.containsKey(unit?.toUpperCase())) {
            iSOUnits[unit?.toUpperCase()]
        } else {
            unit;
        }
    } catch (e: Exception) {
        unit;
    }
}

fun String.toPersianNumber(): String {
    var number: String? = this;

    if (number == null)
        number = "0";

    val persianNumbers = arrayOf("۰", "۱", "۲", "۳", "۴", "۵", "۶", "۷", "۸", "۹");
    if (number.isEmpty()) {
        return "";
    }

    var out: String = "";
    val length: Int = number.length;
    for (i in 0 until length) {
        val chr: Char = number[i];
        out += when {
            chr.isDigit() -> {
                val num: Int = chr.toString().toInt();
                persianNumbers[num];
            }
            chr == '.' -> ","
            else -> chr
        };
    }

    return out
}

//
fun Long.toReial(): String {
    val self: Long? = this;
    var local = Locale("en", "US");
    val fmt = NumberFormat.getInstance(local);

    return if (self == null) {
        "0";
    } else {
        fmt.format(this);
        //String.format("%,d", self.toInt());
    }
}

fun BigDecimal.toReial(): String {
    if (this < BigDecimal(1)) {
        return this.toString();
    }
    val formatter = DecimalFormat("#,###");
    formatter.maximumFractionDigits = Constant.scale;
    formatter.isParseBigDecimal = true;
    formatter.roundingMode = RoundingMode.HALF_EVEN;
    return formatter.format(this);
}

fun Double.toReial(): String {
    val self: Double? = this;
    val local = Locale("en", "US");
    val fmt = NumberFormat.getInstance(local);

    return if (self == null) {
        "0";
    } else {
        fmt.format(this);
        //String.format("%,d", self.toInt());
    }
}

fun isPackageInstalled(packageName: String, packageManager: PackageManager): Boolean {
    var found = true;

    try {
        packageManager.getPackageInfo(packageName, 0);
    } catch (e: PackageManager.NameNotFoundException) {
        found = false;
    }

    return found;
}

fun plusDate(date: Long?, day: Int): Long {
    val c = Calendar.getInstance();
    c.clear();
    c.timeInMillis = date ?: 0;
    c.add(Calendar.DAY_OF_MONTH, day);

    return c.timeInMillis;
}

fun compatibleChartData(mapSources: HashMap<String, ArrayList<Source>>): ArrayList<ArrayList<Source>> {
    val compatibleSource: ArrayList<ArrayList<Source>> = ArrayList();
    for (key in mapSources.keys) {
        mapSources[key]?.let { compatibleSource.add(it) };
    }

    return compatibleSource
}


fun View.setViewBackgroundWithoutResettingPadding(background: Drawable?) {
    val paddingBottom = this.paddingBottom
    val paddingStart = ViewCompat.getPaddingStart(this)
    val paddingEnd = ViewCompat.getPaddingEnd(this)
    val paddingTop = this.paddingTop
    ViewCompat.setBackground(this, background)
    ViewCompat.setPaddingRelative(this, paddingStart, paddingTop, paddingEnd, paddingBottom)
}

fun convertSrcToToman(sources: ArrayList<Source>, src: Source?): BigDecimal {
    val unit = Constant.Symbols.IRT;
    var value = BigDecimal(src?.value ?: 0.0);
    if (src?.unit != unit) {
        for (s in sources) {
            if (s.symbol == src?.unit) {
                value = BigDecimal(src?.value ?: 0.0).times(convertSrcToToman(sources, s));
            }
        }
    }

    return value;
}

fun disableScroll(scrollView: NestedScrollView, disable: Boolean) {
    scrollView.setOnTouchListener { view, motionEvent ->
        return@setOnTouchListener disable
    }
}

fun getScreenHeight(context: Context?): Int {
    val displayMetrics = DisplayMetrics();
    (context as BaseActivity).windowManager.defaultDisplay.getMetrics(displayMetrics);
    return displayMetrics.heightPixels;
}

fun copyToClipboard(context: Context?, text: String) {
    val clipboardManager: ClipboardManager = context?.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager;
    val clip = ClipData.newPlainText("label", text);
    clipboardManager.primaryClip = clip;
}

/**
 * This method converts dp unit to equivalent pixels, depending on device density.
 *
 * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
 * @param context Context to get resources and device specific display metrics
 * @return A float value to represent px equivalent to dp depending on device density
 */
fun convertDpToPixel(dp: Float, context: Context): Float {
    return dp / (context.resources.displayMetrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
}

/**
 * This method converts device specific pixels to density independent pixels.
 *
 * @param px A value in px (pixels) unit. Which we need to convert into db
 * @param context Context to get resources and device specific display metrics
 * @return A float value to represent dp equivalent to px value
 */
fun convertPixelsToDp(px: Float, context: Context): Float {
    return px / (context.resources.displayMetrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
}