package ir.capapp.mobile.services

import co.ronash.pushe.PusheListenerService
import org.json.JSONObject

class PushListener : PusheListenerService() {
    override fun onMessageReceived(json: JSONObject?, messageContent: JSONObject?) {
        super.onMessageReceived(json, messageContent);
    }
}