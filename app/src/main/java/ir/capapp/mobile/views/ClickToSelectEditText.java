package ir.capapp.mobile.views;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Canvas;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.EditText;
import android.widget.ListAdapter;

import com.google.android.material.textfield.TextInputEditText;

import androidx.appcompat.app.AlertDialog;

import java.text.DecimalFormat;

import ir.capapp.mobile.R;
import ir.capapp.mobile.common.NumberTextWatcher;

public class ClickToSelectEditText<T> extends TextInputEditText {

    CharSequence mHint;

    OnItemSelectedListener<T> onItemSelectedListener;
    ListAdapter mSpinnerAdapter;

    public ClickToSelectEditText(Context context) {
        super(context);

        mHint = getHint();
    }

    public ClickToSelectEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        mHint = getHint();
    }

    public ClickToSelectEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        mHint = getHint();
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        setFocusable(true);
        setClickable(true);
        setLongClickable(false);
    }

    public void setAdapter(ListAdapter adapter) {
        mSpinnerAdapter = adapter;

        configureOnClickListener();
    }


    private void configureOnClickListener() {
        addTextChangedListener(new NumberTextWatcher(this));

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(view);
            }
        });


        setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (b) {
                    showDialog(view);
                }
            }
        });
    }

    private void showDialog(View view) {
        setShowSoftInputOnFocus(false);
        AlertDialog.Builder builder = new AlertDialog.Builder(view.getContext());
        builder.setTitle(mHint);
        builder.setAdapter(mSpinnerAdapter, (dialogInterface, selectedIndex) -> {
            if (onItemSelectedListener != null) {
                onItemSelectedListener.onItemSelectedListener((T) mSpinnerAdapter.getItem(selectedIndex), selectedIndex);
            }
        });
        builder.setNeutralButton("لغو", null);
        AlertDialog create = builder.create();
        create.getWindow().getDecorView().setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        create.show();
    }

    public void setOnItemSelectedListener(OnItemSelectedListener<T> onItemSelectedListener) {
        this.onItemSelectedListener = onItemSelectedListener;
    }

    public interface OnItemSelectedListener<T> {
        void onItemSelectedListener(T item, int selectedIndex);
    }
}
