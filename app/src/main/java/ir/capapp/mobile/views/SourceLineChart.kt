package ir.capapp.mobile.views

import android.content.Context
import android.graphics.Color
import android.graphics.DashPathEffect
import android.graphics.Typeface
import android.graphics.drawable.Drawable
import android.util.AttributeSet
import android.util.Log
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.charts.LineChart
import com.github.mikephil.charting.components.Legend
import com.github.mikephil.charting.components.XAxis
import com.github.mikephil.charting.data.Entry
import com.github.mikephil.charting.data.LineData
import com.github.mikephil.charting.data.LineDataSet
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet
import ir.capapp.mobile.R
import ir.capapp.mobile.common.*
import ir.capapp.mobile.constant.Constant
import ir.capapp.mobile.models.Source
import java.lang.Exception

class SourceLineChart(context: Context?, attrs: AttributeSet) : LineChart(context, attrs) {
    var lineSourcesList: ArrayList<ArrayList<Source>> = ArrayList();
    var isComparableChart: Boolean = false;

    private var sourceColors: HashMap<Int, Drawable?> = HashMap();
    private var sources: ArrayList<Source> = ArrayList();


    var font: Typeface? = null;

    init {
        putColorToArray();

        setTouchEnabled(true);
        setBorderColor(Color.TRANSPARENT);
        setBackgroundColor(Color.TRANSPARENT);
        setGridBackgroundColor(Color.TRANSPARENT);
        description.isEnabled = false;
        setNoDataText("داده ای وجود ندارد");
        isDragEnabled = true;
        animateX(1500);
        setPinchZoom(true);
        font = Typeface.createFromAsset(context?.assets, "fonts/Shabnam-Light.ttf");

        xAxis();
        yAxis();
        legend();

        // fix the right padding
        setExtraOffsets(0f, 0f, 30f, 0f)
    }

    fun resetChart() {
        fitScreen();
        putColorToArray();
        lineSourcesList.clear();
        invalidate();
        clear();
    }

    private fun putColorToArray() {
        sourceColors.put(Color.parseColor("#11998e"), ContextCompat.getDrawable(context, R.drawable.fill_color_1));
        sourceColors.put(Color.parseColor("#E93660DC"), ContextCompat.getDrawable(context, R.drawable.fill_color_2));
        sourceColors.put(Color.parseColor("#9F7F0C"), ContextCompat.getDrawable(context, R.drawable.fill_color_3));
    }

    private fun pickGradientColor(): FillColor? {
        var d: Drawable? = null;
        var key = 0;
        for (k in sourceColors.keys) {
            d = sourceColors[k];
            key = k;
            break;
        }
        val drawableIndex = sourceColors.keys.indexOfFirst { it == key };
        if (drawableIndex != -1)
            sourceColors.remove(key);

        val fillColor = FillColor();
        fillColor.color = key;
        fillColor.drawable = d;

        return fillColor;
    }

    fun notifyDataSetChange() {
        setChartData()
    }

    fun addSourcesSummary(sources: ArrayList<Source>) {
        this.sources = sources;
    }

    private fun setChartData() {
        val lineDataSetsList = ArrayList<LineDataSet>();
        for (item in lineSourcesList) {
            item.reverse();
            lineDataSetsList.add(getDataSet(item));
        }
        data = getChartLineData(*lineDataSetsList.toTypedArray());
        setVisibleXRangeMaximum(3f);
        // begin the chart at the right side
        moveViewToX(xChartMax);
    }

    private fun getChartLineData(vararg dataSets: LineDataSet): LineData {
        val finalDataSets = ArrayList<ILineDataSet>();
        for (ds in dataSets) {
            finalDataSets.add(ds);
        }

        val ld = LineData(finalDataSets);
        ld.setValueTypeface(Typeface.createFromAsset(context.assets, "fonts/Shabnam-Light.ttf"));
        ld.setValueFormatter { value, entry, dataSetIndex, viewPortHandler ->
            val unit = entry.data.toString();
            val mUnit: String? = convertISOCurrencyUnitsForDisplay(unit);
            val mValue = value.toDouble().toReial().toPersianNumber();

            return@setValueFormatter "$mValue $mUnit";
        }

        return ld;
    }

    private fun getDataSet(list: List<Source>): LineDataSet {
        fun convertSourcesListToChartEntries(list: List<Source>): ArrayList<Entry> {
            val values = ArrayList<Entry>()

            for (i in 0 until list.size) {
                val source = list[i];

                var unit = source.unit;
                val value = if (isComparableChart && source.unit != Constant.Symbols.IRT) {
                    unit = Constant.Symbols.IRT;
                    convertSrcToToman(sources, source).toFloat()
                } else {
                    source.value.toFloat()
                }

                val entry = Entry(i.toFloat(), value, unit);
                values.add(entry);
            }

            return values;
        }

        val values = convertSourcesListToChartEntries(list);

        return if (data != null && data.dataSetCount > 0) {
            val ds = data.getDataSetByIndex(0) as LineDataSet;
            data.notifyDataChanged();
            notifyDataSetChanged();
            ds;
        } else {
            val ds = LineDataSet(values, list[0].title);
            ds.mode = LineDataSet.Mode.HORIZONTAL_BEZIER;
            ds.cubicIntensity = 0.2f;
            val sourceColor = pickGradientColor();
            ds.fillDrawable = sourceColor?.drawable;
            ds.color = sourceColor?.color ?: 0;
            //ds.color = getThemeColor(context, R.attr.primaryTextColor);
            ds.valueTextColor = getThemeColor(context, R.attr.primaryTextColor);
            ds.setCircleColor(getThemeColor(context, R.attr.colorAccent));
            ds.lineWidth = 1f;
            ds.circleRadius = 3f;
            ds.setDrawCircleHole(false);
            ds.formLineWidth = 1f;
            ds.formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f);
            ds.formSize = 15f;
            ds.valueTextSize = 9f;
            ds.enableDashedHighlightLine(10f, 5f, 0f);
            ds.setDrawFilled(true);
            ds;
        }
    }

    private fun xAxis() {
        val xAxis = xAxis;
        xAxis.typeface = font;
        xAxis.enableGridDashedLine(10f, 10f, 0f);
        xAxis.position = XAxis.XAxisPosition.TOP;
        xAxis.textColor = getThemeColor(context, R.attr.secondaryTextColor);
        xAxis.setDrawGridLines(false);
        xAxis.granularity = 1.0f;
        xAxis.setValueFormatter { value, axis ->
            try {
                return@setValueFormatter lineSourcesList[0][value.toInt()].valueCreatedAt.persianFormatted;
            } catch (e: Exception) {
                return@setValueFormatter "";
            }
        }
    }

    private fun yAxis() {
        val yAxis = axisLeft;
        yAxis.typeface = font;
        yAxis.textColor = getThemeColor(context, R.attr.secondaryTextColor);
        axisRight.isEnabled = false;
        yAxis.enableGridDashedLine(10f, 10f, 0f);
    }


    private fun legend() {
        val legend = legend;
        legend.form = Legend.LegendForm.LINE;
        legend.typeface = font;
        legend.textColor = getThemeColor(context, R.attr.primaryTextColor);
        legend.position = Legend.LegendPosition.BELOW_CHART_RIGHT;
    }


//    chart.description.isEnabled = false;
//    //chart.setBackgroundColor(Color.BLACK);
//    chart.setBorderColor(Color.WHITE);
//    chart.setGridBackgroundColor(Color.WHITE);
//    chart.setNoDataText("داده ای وجود ندارد");
//    chart.setTouchEnabled(true);
//    chart.isDragEnabled = true;
//    chart.setScaleEnabled(true);
//    chart.setPinchZoom(true);
//
//    val font = Typeface.createFromAsset(activity?.assets, "fonts/Shabnam-Light.ttf");
//
//    val xAxis = chart.xAxis;
//    xAxis.typeface = font;
//    xAxis.enableGridDashedLine(10f, 10f, 0f);
//    xAxis.position = XAxis.XAxisPosition.TOP;
//    xAxis.setDrawGridLines(false);
//    xAxis.granularity = 1.0f;
//    //xAxis.textColor = Color.WHITE;
////        xAxis.setValueFormatter { value, axis ->
////            // TODO: Find a solution for time stamps
////            val timeStamp = chart.lineSourcesList[0][value.toInt()].updatedAtTimestamp;
////            val sdf = SimpleDateFormat("yyyy/MM/dd", Locale.US);
////            val localTime = sdf.format(Date(timeStamp * 1000));
////            val jalaliDate = JalaliCalendar().getJalaliDate(sdf.parse(localTime));
////
////            return@setValueFormatter jalaliDate.toPersianNumber();
////        }
//
//    val yAxis = chart.axisLeft;
//    yAxis.typeface = font;
//    //yAxis?.textColor = Color.WHITE;
//    chart.axisRight.isEnabled = false;
//    yAxis.setValueFormatter { value, axis ->
//        return@setValueFormatter value.toReial().toPersianNumber();
//    }
//    yAxis.enableGridDashedLine(10f, 10f, 0f);
//
//    chart.animateX(1500);
//    val legend = chart.legend;
//    legend.form = Legend.LegendForm.LINE;
//    legend.typeface = font;
//    legend.position = Legend.LegendPosition.BELOW_CHART_RIGHT;
}