import org.json.JSONObject

class HttpRequestListeners {

    interface StringResponseListener {

        fun response(response: String?);
        fun error(error: JSONObject?)
    }
}
