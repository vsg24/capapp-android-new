package ir.capapp.mobile.api;

import android.content.Context
import android.webkit.URLUtil
import com.android.volley.Request
import com.android.volley.Response
import ir.capapp.mobile.api.utils.AppClient
import ir.capapp.mobile.api.utils.CustomRequest
import ir.capapp.mobile.common.urlMaker
import org.json.JSONObject
import java.lang.Exception

fun API.Main.Companion.checkForUpdate(context: Context?, listener: ((Boolean?, Int?, Boolean?, Boolean?, String?) -> Unit)?) {
    val url = URLUtil().urlMaker("check-android-update");
    val req = CustomRequest(context, Request.Method.GET, url, JSONObject(),
            Response.Listener<String> { response ->
                try {
                    val obj = JSONObject(response);

                    val updateAvailable = obj.getBoolean("updateAvailable");
                    val newVersionCode = obj.getInt("newVersionCode");
                    val forcedUpdate = obj.getBoolean("forcedUpdate");
                    val link = obj.getString("link");
                    val preferLink = obj.getBoolean("prefer_link");

                    listener?.invoke(updateAvailable, newVersionCode, forcedUpdate, preferLink, link);
                } catch (e: Exception) {
                    e.printStackTrace();
                    listener?.invoke(null, null, null, null, null);
                }
            },
            Response.ErrorListener {
                it.printStackTrace();
                listener?.invoke(null, null, null, null, null);
            }
    );
    AppClient.getInstance(context).addToRequestQueue(req);
}