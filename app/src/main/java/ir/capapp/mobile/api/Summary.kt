package ir.capapp.mobile.api

import android.content.Context
import android.webkit.URLUtil
import com.android.volley.Response
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ir.capapp.mobile.api.utils.AppClient
import ir.capapp.mobile.api.utils.CacheRequest
import ir.capapp.mobile.common.urlMaker
import ir.capapp.mobile.models.PriceChange
import ir.capapp.mobile.models.References
import ir.capapp.mobile.models.Source
import ir.capapp.mobile.models.Special
import org.jetbrains.anko.doAsync
import org.json.JSONObject

fun API.Source.Companion.get(
        context: Context?,
        includeCrypto: Boolean = false,
        listener: ((sources: ArrayList<Source>?,
                    specials: ArrayList<Special>?,
                    priceChanges: ArrayList<PriceChange>?,
                    references: ArrayList<References>?,
                    conversionEnable: Boolean?,
                    error: String?) -> Unit)?) {

    val url = URLUtil().urlMaker("summary", "include_crypto=$includeCrypto & include_user_price_change_notifications=true");
    val req = CacheRequest(context, url,
            Response.Listener<String> { response ->
                try {
                    val master = JSONObject(response);
                    val sourcesJson = master.getString("sources");
                    val specialsJson = master.getString("specials");
                    val priceNotification = master.getString("userPriceChangeNotifications");
                    val referencesJson = master.getString("references");
                    val conversionEnableJson = master.getBoolean("conversionEnabled");

                    val sources: ArrayList<Source> = Gson().fromJson(sourcesJson, object : TypeToken<ArrayList<Source>>() {}.type);
                    val specials: ArrayList<Special> = Gson().fromJson(specialsJson, object : TypeToken<ArrayList<Special>>() {}.type);
                    val prices: ArrayList<PriceChange> = Gson().fromJson(priceNotification, object : TypeToken<ArrayList<PriceChange>>() {}.type);
                    val references: ArrayList<References> = Gson().fromJson(referencesJson, object : TypeToken<ArrayList<References>>() {}.type);

                    listener?.invoke(sources, specials, prices, references, conversionEnableJson, null);
                } catch (e: Exception) {
                    e.printStackTrace();
                }
            },
            Response.ErrorListener { error ->
                error.printStackTrace();
                listener?.invoke(null, null, null, null, null, error.message);
            });

    AppClient.getInstance(context).addToRequestQueue(req);
}

fun API.Source.Companion.history(context: Context?, symbols: HashMap<String, Boolean>, listener: ((sources: ArrayList<ArrayList<Source>>?, error: String?) -> Unit)?) {
    var query = "";

    for (symbol in symbols.keys) {
        query += if (symbols[symbol] == true) {
            "crypto_source[]=${symbol.toUpperCase()}&"
        } else {
            "source[]=${symbol.toUpperCase()}&"
        }
    }

//    if(cryptoSymbols != null) {
//        for (cryptoSymbol in cryptoSymbols) {
//            query += "crypto_source[]=${cryptoSymbol?.toUpperCase()}&"
//        }
//    }

    val url = URLUtil().urlMaker("full-source-history?$query");

    val req = CacheRequest(context, url,
            Response.Listener<String> { response ->
                try {
                    val sources: ArrayList<ArrayList<Source>> = Gson().fromJson(response, object : TypeToken<ArrayList<ArrayList<Source>>>() {}.type);
                    listener?.invoke(sources, null);
                } catch (e: Exception) {
                    e.printStackTrace();
                    listener?.invoke(null, "");
                }
            },
            Response.ErrorListener { error ->
                try {
                    listener?.invoke(null, error.message);
                } catch (e: Exception) {
                    e.printStackTrace();
                    listener?.invoke(null, "");
                }
            });

    AppClient.getInstance(context).addToRequestQueue(req);
}

fun API.Source.Companion.compare(context: Context?, symbols: HashMap<String, Boolean>?, listener: ((sources: ArrayList<ArrayList<Source?>>?, error: String?) -> Unit)?) {
    var query = "";

    if (symbols != null) {
        for (symbol in symbols.keys) {
            query += if (symbols[symbol] == true) {
                "crypto_source[]=${symbol.toUpperCase()}&"
            } else {
                "source[]=${symbol.toUpperCase()}&"
            }
        }
    }

    val url = URLUtil().urlMaker("compare-source-history?$query");
    val req = CacheRequest(context, url,
            Response.Listener<String> { response ->
                try {
                    val sources: ArrayList<ArrayList<Source?>> = Gson().fromJson(response, object : TypeToken<ArrayList<ArrayList<Source?>>>() {}.type);
                    listener?.invoke(sources, null);
                } catch (e: Exception) {
                    e.printStackTrace();
                    listener?.invoke(null, "");
                }
            },
            Response.ErrorListener { error ->
                try {
                    listener?.invoke(null, error.message);
                } catch (e: Exception) {
                    e.printStackTrace();
                    listener?.invoke(null, "");
                }
            });

    AppClient.getInstance(context).addToRequestQueue(req);
}