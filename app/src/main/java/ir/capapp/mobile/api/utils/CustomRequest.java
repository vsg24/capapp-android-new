package ir.capapp.mobile.api.utils;

import android.content.Context;
import android.os.Build;
import com.android.volley.AuthFailureError;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Response;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;
import org.json.JSONArray;
import org.json.JSONObject;
import java.util.HashMap;
import java.util.Map;
import androidx.annotation.Nullable;
import co.ronash.pushe.Pushe;
import ir.capapp.mobile.BuildConfig;

public class CustomRequest extends JsonRequest<String> {

    private Context mContext;

    public CustomRequest(Context context, String url, Response.Listener<String> listener, @Nullable Response.ErrorListener errorListener) {
        super(Method.GET, url, null, listener, errorListener);
        this.mContext = context;
    }

    public CustomRequest(Context context, int method, String url, @Nullable JSONArray requestBody, Response.Listener<String> listener, @Nullable Response.ErrorListener errorListener) {
        super(method, url, requestBody == null ? null : requestBody.toString(), listener, errorListener);
        this.mContext = context;
    }

    public CustomRequest(Context context, int method, String url, @Nullable JSONObject requestBody, Response.Listener<String> listener, @Nullable Response.ErrorListener errorListener) {
        super(method, url, requestBody == null ? null : requestBody.toString(), listener, errorListener);
        this.mContext = context;
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        try {
            headers.put("INSTANCE-ID", Pushe.getPusheId(mContext));
        } catch (Exception ignored) { }
        try {
            headers.put("APP-VERSION-CODE", String.valueOf(BuildConfig.VERSION_CODE));
            headers.put("APP-VERSION-NAME", BuildConfig.VERSION_NAME);
            headers.put("APP-BUILD-TYPE", BuildConfig.BUILD_TYPE);
            headers.put("DEVICE-BRAND", Build.BRAND);
            headers.put("DEVICE-MANUFACTURER", Build.MANUFACTURER);
            headers.put("OS-VERSION-CODE", String.valueOf(Build.VERSION.SDK_INT));
            headers.put("OS-VERSION-NAME", Build.VERSION.RELEASE);
        } catch (Exception ignored) {}

        //headers.put("", String.valueOf(new Setting(mContext).getDarkMode()));

        return headers;
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        try {
            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
            return Response.success(jsonString, HttpHeaderParser.parseCacheHeaders(response));
        } catch (Exception e) {
            return Response.error(new ParseError(e));
        }
    }
}
