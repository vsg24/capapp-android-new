package ir.capapp.mobile.api.utils;

import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.Cache;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.HttpHeaderParser;
import com.android.volley.toolbox.JsonRequest;

import java.util.HashMap;
import java.util.Map;

import co.ronash.pushe.Pushe;
import ir.capapp.mobile.activity.BaseActivity;
import ir.capapp.mobile.activity.SubscriptionActivity;

public class CacheRequest extends JsonRequest<String> {
    private final static int TIME_OUT = 30000;
    private Context mContext;

    public CacheRequest(Context context, String url, Response.Listener<String> listener, Response.ErrorListener errorListener) {
        super(Method.GET, url, null, listener, errorListener);
        mContext = context;
    }

    @Override
    public Request<?> setRetryPolicy(RetryPolicy retryPolicy) {
        return super.setRetryPolicy(new DefaultRetryPolicy(
                TIME_OUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        ));
    }

    @Override
    public Map<String, String> getHeaders() throws AuthFailureError {
        Map<String, String> headers = new HashMap<>();
        try {
            headers.put("INSTANCE-ID", Pushe.getPusheId(mContext));
        } catch (Exception ignored) { }

        return headers;
    }

    @Override
    protected VolleyError parseNetworkError(VolleyError volleyError) {
        if(volleyError.networkResponse != null && volleyError.networkResponse.statusCode == 401) {
            Intent intent = new Intent(mContext, SubscriptionActivity.class);
            intent.putExtra("subscribe", true);
            mContext.startActivity(intent);
            ((BaseActivity)mContext).finish();
        }


        return super.parseNetworkError(volleyError);
    }

    @Override
    protected Response<String> parseNetworkResponse(NetworkResponse response) {
        Cache.Entry cacheEntry = HttpHeaderParser.parseCacheHeaders(response);
        if (cacheEntry == null) {
            cacheEntry = new Cache.Entry();
        }
        final long cacheHitButRefreshed = 60 * 1000;
        final long cacheExpired = 60 * 1000;
        long now = System.currentTimeMillis();
        final long softExpire = now + cacheHitButRefreshed;
        final long ttl = now + cacheExpired;
        cacheEntry.data = response.data;
        cacheEntry.softTtl = softExpire;
        cacheEntry.ttl = ttl;
        String headerValue;
        headerValue = response.headers.get("Date");
        if (headerValue != null) {
            cacheEntry.serverDate = HttpHeaderParser.parseDateAsEpoch(headerValue);
        }
        headerValue = response.headers.get("Last-Modified");
        if (headerValue != null) {
            cacheEntry.lastModified = HttpHeaderParser.parseDateAsEpoch(headerValue);
        }
        cacheEntry.responseHeaders = response.headers;

        try {
            String jsonString = new String(response.data, HttpHeaderParser.parseCharset(response.headers, PROTOCOL_CHARSET));
            return Response.success(jsonString, cacheEntry);
        } catch (Exception e) {
            return Response.error(new ParseError(e));
        }
    }
}
