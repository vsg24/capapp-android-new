package ir.capapp.mobile.api

import android.content.Context
import android.util.Log
import android.webkit.URLUtil
import com.android.volley.NetworkResponse
import com.android.volley.Request
import com.android.volley.Response
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ir.capapp.mobile.api.utils.AppClient
import ir.capapp.mobile.api.utils.CustomRequest
import ir.capapp.mobile.common.urlMaker
import ir.capapp.mobile.models.FreeTrial
import ir.capapp.mobile.models.InAppPurchasableProductPurchaseResult
import ir.capapp.mobile.models.PriceChange
import ir.capapp.mobile.models.ServerPurchaseResult
import org.json.JSONObject
import java.nio.charset.Charset

fun API.Subscription.Companion.checkPurchase(context: Context?, purchaseData: InAppPurchasableProductPurchaseResult, listener: ((result: ServerPurchaseResult?, error: String?) -> Unit)?) {
    val requestObj = JSONObject(Gson().toJson(purchaseData));

    val url = URLUtil().urlMaker("subscription/check-purchase");
    val req = CustomRequest(context, Request.Method.POST, url, requestObj,
            Response.Listener<String> { response ->
                try {
                    val result: ServerPurchaseResult = Gson().fromJson(response, object : TypeToken<ServerPurchaseResult>() {}.type);
                    listener?.invoke(result, null);
                } catch (e: Exception) {
                    e.printStackTrace();
                }
            },
            Response.ErrorListener { error ->
                 listener?.invoke(null, error.message);
            });

    AppClient.getInstance(context).addToRequestQueue(req);
}

fun API.Subscription.Notification.Companion.priceChange(context: Context?, priceChange: PriceChange, listener: ((String?) -> Unit)?) {
    val url = URLUtil().urlMaker("subscription/price-change-notification");
    val req = CustomRequest(context, Request.Method.POST, url, JSONObject(Gson().toJson(priceChange)),
            Response.Listener {
                try {
                    listener?.invoke(null);
                }catch (e: java.lang.Exception) {
                    e.printStackTrace();
                    listener?.invoke(e.message);
                }
            },
            Response.ErrorListener {
                it.printStackTrace();
                listener?.invoke(it.message);
            }
    );
    AppClient.getInstance(context).addToRequestQueue(req);
}

fun API.Subscription.Notification.Companion.priceChangeDelete(context: Context?, symbol: String?, listener: ((String?) -> Unit)?) {
    val url = URLUtil().urlMaker("subscription/price-change-notification", "symbol=$symbol");
    val req = CustomRequest(context, Request.Method.DELETE, url, JSONObject(),
            Response.Listener {
                try {
                    listener?.invoke(null);
                } catch (e: java.lang.Exception) {
                    e.printStackTrace();
                    listener?.invoke(e.message);
                }
            },
            Response.ErrorListener {
                it.printStackTrace();
                listener?.invoke(it.message);
            }
    );
    AppClient.getInstance(context).addToRequestQueue(req);
}

fun API.Subscription.Companion.getSKUWithUserSub(context: Context?, listener: ((isSubscriptionValid: Boolean?, serverPurchaseResult: ServerPurchaseResult?, String?, skuList: ArrayList<String>?, FreeTrial?) -> Unit)?) {
    val url = URLUtil().urlMaker("subscription/sku-with-user-sub");
    val req = CustomRequest(context, Request.Method.POST, url, JSONObject(),
            Response.Listener<String> { response ->
                try {
                    val subStartAndFinishDate: ServerPurchaseResult = Gson().fromJson(response, object : TypeToken<ServerPurchaseResult>() {}.type);
                    val obj = JSONObject(response);

                    val skuItems = ArrayList<String>();
                    for (i in 0 until obj.getJSONArray("skus").length()) {
                        skuItems.add(obj.getJSONArray("skus").getString(i));
                    }

                    val rsa = obj.getString("rsa");
                    val freeTrial: FreeTrial? = if(obj.has("freeTrial")) Gson().fromJson(obj.getString("freeTrial"), FreeTrial::class.java) else null;

                    listener?.invoke(obj.getBoolean("subscriptionValid"), subStartAndFinishDate, rsa, skuItems, freeTrial);
                } catch (e: Exception) {
                    e.printStackTrace();
                }
            },
            Response.ErrorListener { error ->
                error.printStackTrace();
                listener?.invoke(null, null, null, null, null);
            });

    AppClient.getInstance(context).addToRequestQueue(req);
}