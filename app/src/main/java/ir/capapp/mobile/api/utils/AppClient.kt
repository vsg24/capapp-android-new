package ir.capapp.mobile.api.utils

import android.content.Context
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Request
import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley


class AppClient constructor(context: Context?) {
    companion object {
        @Volatile
        private var INSTANCE: AppClient? = null

        fun getInstance(context: Context?) =
                INSTANCE ?: synchronized(this) {
                    INSTANCE ?: AppClient(context).also {
                        INSTANCE = it;
                    }
                }
    }

    private val requestQueue: RequestQueue by lazy {
        // applicationContext is key, it keeps you from leaking the
        // Activity or BroadcastReceiver if someone passes one in.
        Volley.newRequestQueue(context?.applicationContext)
    }

    fun clearRequestQueue() {
        requestQueue.cache.clear();
    }

    fun <T> addToRequestQueue(req: Request<T>) {
        req.retryPolicy = DefaultRetryPolicy(
                3000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT
        )

        requestQueue.add(req)
    }
}