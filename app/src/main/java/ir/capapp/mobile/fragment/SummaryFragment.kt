package ir.capapp.mobile.fragment

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.cooltechworks.views.shimmer.ShimmerRecyclerView
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.button.MaterialButton
import com.google.gson.Gson
import io.supercharge.shimmerlayout.ShimmerLayout
import ir.capapp.mobile.activity.MainActivity
import ir.capapp.mobile.activity.SingleSourceActivity
import ir.capapp.mobile.adapter.SourceListAdapter
import ir.capapp.mobile.adapter.TinyPriceListAdapter
import ir.capapp.mobile.api.API
import ir.capapp.mobile.api.compare
import ir.capapp.mobile.common.*
import ir.capapp.mobile.constant.Constant
import ir.capapp.mobile.models.Source
import ir.capapp.mobile.repository.SourceRepository
import ir.capapp.mobile.views.SourceLineChart
import kotlin.collections.ArrayList
import kotlin.collections.HashMap
import kotlin.collections.arrayListOf
import kotlin.collections.forEach
import ir.capapp.mobile.R
import ir.capapp.mobile.activity.BaseActivity
import ir.capapp.mobile.api.utils.AppClient
import kotlin.collections.indexOfFirst
import kotlin.collections.map
import kotlin.collections.set


class SummaryFragment : Fragment() {

    private lateinit var specialsRecyclerView: ShimmerRecyclerView;
    private lateinit var sourcesRecyclerView: ShimmerRecyclerView;
    private lateinit var capitalValue: TextView;
    private lateinit var summarySourceLabel: TextView;
    private lateinit var chart: SourceLineChart;
    private lateinit var chartShimmerLayout: ShimmerLayout;
    private lateinit var emptyListContainer: ConstraintLayout;
    private lateinit var refreshLayout: SwipeRefreshLayout;

    private var specialsListAdapter: TinyPriceListAdapter? = null;
    private var sourcesListAdapter: SourceListAdapter? = null;
    private var list: ArrayList<Source?> = ArrayList();
    private var mySources = ArrayList<Source>();
    private var sourceRepository = SourceRepository();
    private var isSourcesReady: Boolean = false;
    private var isChartReady: Boolean = false;
    private var user: UserManager? = null;
    private var canSetItemTouchHelper: Boolean = true;

    private val itemTouchCallBack = object : ItemTouchHelper.SimpleCallback(
            ItemTouchHelper.UP or ItemTouchHelper.DOWN, ItemTouchHelper.LEFT) {

        var dragFrom: Int = -1;
        var dragTo: Int = -1;

        override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {

            val firstPosition = viewHolder.adapterPosition;
            val secondPosition = target.adapterPosition;

            if (dragFrom == -1)
                dragFrom = firstPosition;
            dragTo = secondPosition;
            sourcesListAdapter?.notifyItemMoved(firstPosition, secondPosition);

            return true;
        }

        override fun isLongPressDragEnabled(): Boolean {
            return true;
        }

        private fun drop(firstPosition: Int, secondPosition: Int) {
        }

        override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
            super.clearView(recyclerView, viewHolder);

            if (dragFrom != -1 && dragTo != -1 && dragFrom != dragTo)
                drop(dragFrom, dragTo);

            dragFrom = -1;
            dragTo = dragFrom;
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
            try {
                val position = viewHolder.adapterPosition;
                val item = list[position] as Source;
                val mSourceIndex = mySources.indexOfFirst { it.symbol == item.symbol }
                sourceRepository.delete(item);
                list.removeAt(position);
                if (mSourceIndex != -1)
                    mySources.removeAt(mSourceIndex);
                sourcesListAdapter?.notifyItemRemoved(position);
                val temp = ArrayList<Source>();
                list.forEach {
                    if (it != null)
                        temp.add(it);
                }
                initCapital(temp);
                notifyPage();
            } catch (e: Exception) {
                e.printStackTrace();
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v = inflater.inflate(R.layout.fragment_summary, container, false);
        specialsRecyclerView = v.findViewById(R.id.specialsRecyclerView);
        sourcesRecyclerView = v.findViewById(R.id.summary_list);
        chart = v.findViewById(R.id.summary_line_chart);
        chartShimmerLayout = v.findViewById(R.id.summary_line_chart_shimmer);
        emptyListContainer = v.findViewById(R.id.summary_empty_list_container);
        refreshLayout = v.findViewById(R.id.summary_refresh);

        chart.isComparableChart = true;

        refreshLayout.setOnRefreshListener {
            refreshLayout.isRefreshing = false;
            (context as BaseActivity).recreate();
            AppClient(context).clearRequestQueue();
        }
        emptyListContainer.setOnClickListener {
            helperDialog();
        }

        user = UserManager(context);

        val scrollView: NestedScrollView = v.findViewById(R.id.scrollView);
        val appBarLayout: AppBarLayout? = activity?.findViewById(R.id.main_app_bar);
        capitalValue = v.findViewById(R.id.capital_value);
        summarySourceLabel = v.findViewById(R.id.summary_source);

        // lift on scroll
        scrollView.setOnScrollChangeListener(AppBarLayoutElevationScrollView(appBarLayout));

        mySources.addAll(sourceRepository.findAll());
        initList();
        newRequest();

        chartShimmerLayout.startShimmerAnimation();

        return v;
    }

    private fun newRequest() {
        list.clear();
        isSourcesReady = false;
        isChartReady = false;
        sourcesListAdapter?.notifyDataSetChanged();

        //val offlineSources = readCacheData(Constant.Action.SOURCE);
//        if(offlineSources != null) {
//            val sources: ArrayList<Source> = Gson().fromJson(offlineSources, object : TypeToken<ArrayList<Source>>(){}.type);
//            margeSources(sources);
//            notifyPage();
//        } else {
        getSummary();
//        }

        getChartData();
    }

    private fun getChartData() {

        val symbols = user?.symbols ?: HashMap();

        API.Source.compare(context, symbols) { sources, error ->
            if (sources != null) {
                val compatibleSource = compatibleChartData(mapChartSources(sources));

                chart.lineSourcesList = compatibleSource;
                chart.notifyDataSetChange();
                //setChartData(*compatibleSource.toTypedArray());
            }
        };
    }

    private fun mapChartSources(sources: ArrayList<ArrayList<Source?>>?): HashMap<String, ArrayList<Source>> {
        val mapSources: HashMap<String, ArrayList<Source>> = HashMap();
        if (sources != null) {

            for (i in 0 until sources.size) {
                for (source in sources[i]) {
                    if (source == null)
                        continue;

                    val symbol = source.symbol;
                    if (mapSources.containsKey(symbol)) {
                        mapSources[symbol]?.add(source);
                    } else {
                        val sourceArray = arrayListOf(source);
                        mapSources[symbol] = sourceArray;
                    }
                }
            }
        }

        return mapSources;
    }

    private fun getSummary() {
        (context as MainActivity).getSource { sources, specials, price, references, error ->
            if (sources != null) {
                isSourcesReady = true;
                if (specials != null) {
                    specialsListAdapter?.list = specials;
                    specialsListAdapter?.notifyDataSetChanged();
                }
                chart.addSourcesSummary(sources);
                margeSources(sources);
//                    writeCacheData(sources, Constant.Action.SOURCE);
            } else {

            }
        }
    }

    private fun margeSources(sources: ArrayList<Source>) {
        for (source in sources) {
            val symbolIndex = sources.indexOfFirst { source.unit == it.symbol };
            if (symbolIndex != -1) {
                val dependentSource = sources[symbolIndex];
                source.tempValue = dependentSource.value * source.value;
            }
            for (mySource in mySources) {
                if (mySource.symbol == source.symbol) {
                    source.capital = mySource.capital;
                    if (source.capital > 0.0) {
                        list.add(source);
                    }
                }
            }
        }

        notifyPage();
        initCapital(sources);
    }

    private fun notifyPage() {
        if (canSetItemTouchHelper) {
            val itemTouchHelper = ItemTouchHelper(itemTouchCallBack);
            itemTouchHelper.attachToRecyclerView(sourcesRecyclerView);
            canSetItemTouchHelper = false;
        }

        if (list.size != 0) {
            summarySourceLabel.visibility = View.VISIBLE;
            emptyListContainer.visibility = View.GONE;
        } else {
            summarySourceLabel.visibility = View.GONE;
            emptyListContainer.visibility = View.VISIBLE;
        }
        sourcesRecyclerView.hideShimmerAdapter();
        specialsRecyclerView.hideShimmerAdapter();

        chartShimmerLayout.visibility = View.GONE;
        chart.visibility = View.VISIBLE;

        sourcesListAdapter?.notifyDataSetChanged();
        specialsListAdapter?.notifyDataSetChanged();
    }

    private fun helperDialog() {
        val dialog = AlertDialog.Builder(context).create();
        val view: View = LayoutInflater.from(context).inflate(R.layout.dialog_add_capital, null, false);
        val button = view.findViewById<MaterialButton>(R.id.dialog_gift_submit);

        button.setOnClickListener {
            (activity as MainActivity).openSourceFragment();
            dialog.dismiss();
        }

        dialog.setView(view);
        dialog.show();
    }

//    private fun writeCacheData(data: ArrayList<Source>, key: String) {
//        val sourcesJson = Gson().toJson(data);
//        val pref = SharedPreferencesHelper(context);
//
//        val sourceDataJson = JSONObject();
//        sourceDataJson.put("expirationDate", Constant.Cache.expiresOn);
//        sourceDataJson.put("data", sourcesJson);
//
//        pref.save(key, sourceDataJson.toString());
//    }

//    private fun readCacheData(key: String): String? {
//        val pref = SharedPreferencesHelper(context);
//        val sources = pref.getString(key, "");
//
//        if (sources != "") {
//            val sourcesJson = JSONObject(sources);
//            if (sourcesJson.getLong("expirationDate") > now)
//                return sourcesJson.getString("data");
//        }
//
//        return null;
//    }

    private fun initCapital(sources: ArrayList<Source>) {
        var sum = 0.0;
        sources.map {
            if (it.capital != null && it.value != null) {
                sum += (it.capital * if (it.tempValue != null) it.tempValue else it.value);
            }
        }
        capitalValue.text = sum.toReial();
    }

    private fun initList() {
        specialsListAdapter = TinyPriceListAdapter(context, ArrayList(), specialsRecyclerView);
        sourcesListAdapter = SourceListAdapter(context, list, false, true);
        sourcesListAdapter?.onItemClickListener { view, i ->
            val item = list[i];
            val intent = Intent(context, SingleSourceActivity::class.java);
            intent.putExtra("source", Gson().toJson(item));
            startActivity(intent);
        }

        specialsRecyclerView.layoutManager = GridLayoutManager(context, 2);
        specialsRecyclerView.addItemDecoration(SpacesItemDecoration(convertDpToPixel(16f, context!!).toInt()));
        specialsRecyclerView.adapter = specialsListAdapter;
        specialsRecyclerView.showShimmerAdapter();

        sourcesRecyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        sourcesRecyclerView.itemDecoration(context, R.drawable.inset_right_divider);
        sourcesRecyclerView.adapter = sourcesListAdapter;
        sourcesRecyclerView.showShimmerAdapter();

        chartShimmerLayout.visibility = View.VISIBLE;
        chart.visibility = View.GONE;
        //recyclerView.bouncyOverScroll();
    }

//    private fun getChartLineData(vararg dataSets: LineDataSet): LineData {
//        val finalDataSets = ArrayList<ILineDataSet>();
//        for(ds in dataSets) {
//            finalDataSets.add(ds);
//        }
//
//        val ld = LineData(finalDataSets);
//        ld.setValueTypeface(Typeface.createFromAsset(activity?.assets, "fonts/Shabnam-Light.ttf"));
//        ld.setValueFormatter { value, entry, dataSetIndex, viewPortHandler ->
//            return@setValueFormatter value.toReial().toPersianNumber();
//        }
//
//        return ld;
//    }

//    private fun getDataSet(list: List<Source>): LineDataSet {
//        fun convertSourcesListToChartEntries(list: List<Source>): ArrayList<Entry> {
//            val values = ArrayList<Entry>()
//
//            for (i in 0 until list.size) {
//                val source = list[i];
//                val entry = Entry(i.toFloat() , source.value);
//                values.add(entry);
//            }
//
//            return values;
//        }
//
//        val values = convertSourcesListToChartEntries(list);
//
//        return if (chart.data != null && chart.data.dataSetCount > 0) {
//            val ds = chart.data.getDataSetByIndex(0) as LineDataSet;
//            chart.data.notifyDataChanged();
//            chart.notifyDataSetChanged();
//            ds;
//        } else {
//            val ds = LineDataSet(values, list[0].title);
//            ds.mode = LineDataSet.Mode.HORIZONTAL_BEZIER;
//            ds.cubicIntensity = 0.2f;
//            //ds.color = Color.WHITE;
//            //ds.valueTextColor = Color.WHITE;
//            //ds.setCircleColor(Color.WHITE);
//            ds.lineWidth = 1f;
//            ds.circleRadius = 3f;
//            ds.setDrawCircleHole(false);
//            ds.formLineWidth = 1f;
//            ds.formLineDashEffect = DashPathEffect(floatArrayOf(10f, 5f), 0f);
//            ds.formSize = 15f;
//            ds.valueTextSize = 9f;
//            ds.enableDashedHighlightLine(10f, 5f, 0f);
//            ds.setDrawFilled(true);
//            ds;
//        }
//    }

//    private fun setChartData(vararg list: ArrayList<Source>) {
//        chart.lineSourcesList.clear();
//        chart.lineSourcesList.addAll(list);
//        val lineDataSetsList = ArrayList<LineDataSet>();
//        for(item in chart.lineSourcesList) {
//            lineDataSetsList.add(getDataSet(item));
//        }
//        chart.data = getChartLineData(*lineDataSetsList.toTypedArray());
//        chart.setVisibleXRangeMaximum(2f);
//    }

}