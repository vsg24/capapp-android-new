package ir.capapp.mobile.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import ir.capapp.mobile.BuildConfig
import ir.capapp.mobile.adapter.interfaces.OnItemClickListener
import ir.capapp.mobile.common.Setting
import ir.capapp.mobile.databinding.FragmentSettingBinding
import ir.capapp.mobile.viewmodel.SettingViewModel
import android.content.Intent
import android.net.Uri
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import ir.capapp.mobile.adapter.ReferencesAdapter
import ir.capapp.mobile.common.ServerSetting
import android.widget.Toast
import co.ronash.pushe.Pushe
import ir.capapp.mobile.R
import ir.capapp.mobile.common.copyToClipboard

class SettingFragment : Fragment() {

    private var binding: FragmentSettingBinding? = null;
    private var settingViewModel = SettingViewModel();
    private var adapter: ReferencesAdapter? = null;

    lateinit var recyclerView: RecyclerView;

    private val onItemClickListener = object : OnItemClickListener {
        override fun clicked(view: View, position: Int) {
            try {
                if (position == 1) {
                    val sendIntent = Intent();
                    sendIntent.action = Intent.ACTION_SEND;
                    sendIntent.putExtra(Intent.EXTRA_TEXT, "مدیریت منابع مالی - کپ - https://cafebazaar.ir/app/ir.capapp.mobile");
                    sendIntent.type = "text/plain";
                    startActivity(sendIntent);
                } else if (position == -1) {
                    copyToClipboard(context, Pushe.getPusheId(context));
                    Toast.makeText(context, "کد شناسایی ذخیره شد", Toast.LENGTH_SHORT).show();
                } else {
                    val intent = Intent(Intent.ACTION_VIEW, Uri.parse("mailto:" + "hello@capapp.ir"));
                    intent.putExtra(Intent.EXTRA_SUBJECT, "ارتباط با پشتیبانی");
                    intent.putExtra(Intent.EXTRA_TEXT, "");
                    startActivity(intent);
                }
            } catch (ignore: Exception) {
                ignore.printStackTrace();
            }
        }
    }

    private fun getReferences() {
        val references = ServerSetting.getInstance().references;

        settingViewModel.references = references.size > 0;

        if(references.size > 0) {
            adapter = ReferencesAdapter(context, references);
            recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false);
            recyclerView.adapter = adapter;
            adapter?.notifyDataSetChanged();
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = FragmentSettingBinding.inflate(inflater, container, false);
        settingViewModel.darkMode = Setting(context).darkMode;
        settingViewModel.version = BuildConfig.VERSION_NAME + " (${BuildConfig.VERSION_CODE})";

        recyclerView = binding?.root?.findViewById(R.id.setting_advertisement_list) as RecyclerView;

        getReferences();

        binding?.listener = onItemClickListener;
        binding?.viewModel = settingViewModel;
        val v = binding?.root;

        return v;
    }
}