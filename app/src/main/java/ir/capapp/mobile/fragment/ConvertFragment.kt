package ir.capapp.mobile.fragment

import android.os.Bundle
import android.text.Html
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import androidx.core.text.HtmlCompat
import androidx.fragment.app.Fragment
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import ir.capapp.mobile.R
import ir.capapp.mobile.adapter.SourceCurrencyAdapter
import ir.capapp.mobile.common.NumberTextWatcher
import ir.capapp.mobile.common.convertSrcToToman
import ir.capapp.mobile.common.toReial
import ir.capapp.mobile.constant.Constant
import ir.capapp.mobile.models.Source
import ir.capapp.mobile.views.ClickToSelectEditText
import java.lang.Exception
import java.math.BigDecimal
import java.math.RoundingMode

class ConvertFragment : Fragment() {

    private val sources = ArrayList<Source>();


    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_convert, container, false);
        sources.addAll(Gson().fromJson(arguments?.getString("SOURCE"), object : TypeToken<ArrayList<Source>>() {}.type));

        setupUI(view);

        return view;
    }

    private fun setupUI(v: View) {
        val input: TextInputEditText? = v.findViewById(R.id.input);
        val replaceButton: ImageButton? = v.findViewById(R.id.replaceValue);

        input?.setSelectAllOnFocus(true);
        input?.addTextChangedListener(NumberTextWatcher(input));

        val sourceCurrency: ClickToSelectEditText<Source>? = v.findViewById(R.id.sourceValue);

        val sourceJson = Gson().toJson(sources);
        val sourcesArrayList: ArrayList<Source> = Gson().fromJson(sourceJson, object : TypeToken<ArrayList<Source>>() {}.type);
        val source = Source();
        source.title = "تومان";
        source.symbol = Constant.Symbols.IRT;
        source.value = 1.0;
        sourcesArrayList.add(0, source);

        val sourceTitlesAdapter = SourceCurrencyAdapter(context, sourcesArrayList);

        var srcSource: Source? = null;
        var dstSource: Source? = null;

        sourceCurrency?.setAdapter(sourceTitlesAdapter);
        sourceCurrency?.setOnItemSelectedListener { _item, _ ->
            sourceCurrency.setText(_item.title);
            srcSource = _item;
        }

        val destinationCurrency: ClickToSelectEditText<Source>? = v.findViewById(R.id.destinationValue);

        destinationCurrency?.setAdapter(sourceTitlesAdapter);
        destinationCurrency?.setOnItemSelectedListener { _item, _ ->
            destinationCurrency.setText(_item.title);

            dstSource = _item;
        }

        val result: TextView? = v.findViewById(R.id.result);

        fun calculateCurrency() {
            try {
                val destPrice: BigDecimal;

                val dstTitle = destinationCurrency?.text ?: "";

                val inputVal = BigDecimal(if (input?.text?.toString()?.trim() != "") input?.text?.toString()?.trim()?.replace(",", "") else "0");

                val srcValue = convertSrcToToman(sources, srcSource).multiply(inputVal);
                val destValue = convertSrcToToman(sources, dstSource);
                destPrice = srcValue.divide(destValue, Constant.scale, RoundingMode.HALF_EVEN);


//                destPrice = if (srcSource?.type == "crypto" || dstSource?.type == "crypto") {
//                    val dollar = sources.find { it.symbol == Constant.Symbols.USD };
//
//                    if (dstSource?.symbol == Constant.Symbols.USD) {
//                        inputVal.multiply(BigDecimal(srcSource?.value ?: 0.0));
//                    } else if (srcSource?.symbol == Constant.Symbols.USD) {
//                        inputVal.divide(BigDecimal(dstSource?.value
//                                ?: 0.0), 5, RoundingMode.HALF_EVEN);
//                    } else {
//                        if (dstSource?.type == "crypto" && srcSource?.type != "crypto") {
//                            val originPriceToToman = BigDecimal(dstSource?.value
//                                    ?: 0.0).multiply(BigDecimal(dollar?.value ?: 0.0));
//                            BigDecimal(srcSource?.value
//                                    ?: 0.0).multiply(inputVal).divide(originPriceToToman, 5, RoundingMode.HALF_EVEN);
//                        } else if (dstSource?.type != "crypto" && srcSource?.type == "crypto") {
//
//                            val originPriceToToman = BigDecimal(srcSource?.value
//                                    ?: 0.0).multiply(inputVal).multiply(BigDecimal(dollar?.value
//                                    ?: 0.0));
//                            originPriceToToman.divide(BigDecimal(dstSource?.value
//                                    ?: 0.0), 5, RoundingMode.HALF_EVEN);
//                        } else {
//                            val srcPriceToToman = BigDecimal(srcSource?.value
//                                    ?: 0.0).multiply(inputVal).multiply(BigDecimal(dollar?.value
//                                    ?: 0.0));
//                            val dstPriceToToman = BigDecimal(dstSource?.value
//                                    ?: 0.0).multiply(BigDecimal(dollar?.value ?: 0.0));
//                            srcPriceToToman.divide(dstPriceToToman, 5, RoundingMode.HALF_EVEN);
//                        }
//                    }
//                } else {
//                    if (srcSource?.symbol == Constant.Symbols.IRT && dstSource?.symbol != Constant.Symbols.IRT) {
//                        inputVal.divide(BigDecimal(dstSource?.value
//                                ?: 0.0), 5, RoundingMode.HALF_EVEN);
//                    } else if (dstSource?.symbol == Constant.Symbols.IRT && srcSource?.symbol != Constant.Symbols.IRT) {
//                        inputVal.multiply(BigDecimal(srcSource?.value ?: 0.0));
//                    } else if (srcSource?.symbol == Constant.Symbols.IRT && dstSource?.symbol == Constant.Symbols.IRT) {
//                        inputVal;
//                    } else {
//                        val originPriceToToman = BigDecimal(srcSource?.value
//                                ?: 0.0).multiply(inputVal);
//                        originPriceToToman.divide(BigDecimal(dstSource?.value
//                                ?: 0.0), 5, RoundingMode.HALF_EVEN);
//                    }
//                }

                val pieces = destPrice.toReial().split('.');
                val fraction = if(pieces.size == 1) "" else ("." + pieces[1]);
                result?.text = HtmlCompat.fromHtml("<big><b>" + pieces[0] + "</b></big>" + "<small>" + fraction + "</small>" + " " + dstTitle, HtmlCompat.FROM_HTML_MODE_LEGACY);

                if (result?.text?.first() == '.') {
                    result.text = "0" + result.text;
                }
            } catch (e: Exception) {
                e.printStackTrace();
            }
        }

        replaceButton?.setOnClickListener {
            val temp = sourceCurrency?.text;
            val srcTemp = srcSource;

            srcSource = dstSource;
            dstSource = srcTemp;

            sourceCurrency?.text = destinationCurrency?.text;
            destinationCurrency?.text = temp;

            calculateCurrency();
        }

        val convertButton: MaterialButton? = v.findViewById<MaterialButton?>(R.id.convert);
        convertButton?.setOnClickListener {
            calculateCurrency();
        }
    }
}