package ir.capapp.mobile.fragment

import android.annotation.SuppressLint
import android.app.Dialog
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.coordinatorlayout.widget.CoordinatorLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.gson.Gson
import ir.capapp.mobile.R
import ir.capapp.mobile.adapter.interfaces.OnItemClickListener
import ir.capapp.mobile.common.ServerSetting
import ir.capapp.mobile.common.UserManager
import ir.capapp.mobile.constant.Constant
import ir.capapp.mobile.databinding.FragmentBottomSheetSourceBinding
import ir.capapp.mobile.models.Source
import ir.capapp.mobile.repository.SourceRepository
import ir.capapp.mobile.viewmodel.SourceViewModel

class SourceBottomSheet : BottomSheetDialogFragment() {

    private var serverChart = ServerSetting.getInstance().fallbackSourceChart;
    private var binding: FragmentBottomSheetSourceBinding? = null;
    private var source: Source = Source();
    private val sourceRepository = SourceRepository();
    private val sourceViewModel = SourceViewModel();
    private var mListener: ((data: Source?) -> Unit)? = null;
    private var user: UserManager? = null;

    private var bottomSheetCallBack = object : BottomSheetBehavior.BottomSheetCallback() {
        override fun onSlide(p0: View, p1: Float) {

        }

        override fun onStateChanged(p0: View, p1: Int) {
            if(p1 == BottomSheetBehavior.STATE_HIDDEN) {
                dismiss();
            }
        }
    }

    private var onItemClickListener = object : OnItemClickListener {
        override fun clicked(view: View, position: Int) {
            val source = sourceViewModel.source;
            if(position == 0) {
                sourceRepository.insertOrReplace(sourceViewModel.source);
                mListener?.invoke(source);
                dismiss();
            } else if (position == 1) {
                changeSummaryChartsState(source);
                mListener?.invoke(source);
            }
        }
    }

    @SuppressLint("RestrictedApi")
    override fun setupDialog(dialog: Dialog?, style: Int) {
        super.setupDialog(dialog, style);
        binding = FragmentBottomSheetSourceBinding.inflate(LayoutInflater.from(context), null, false);
        dialog?.setContentView(binding!!.root);
        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);

        val params: CoordinatorLayout.LayoutParams = (binding?.root?.parent as View).layoutParams as CoordinatorLayout.LayoutParams;
        val behavior: BottomSheetBehavior<CoordinatorLayout> = params.behavior as BottomSheetBehavior<CoordinatorLayout>;
        behavior.setBottomSheetCallback(bottomSheetCallBack);

        user = UserManager(context);

        val sourceJson = arguments?.getString(Constant.Action.SOURCE);
        if (sourceJson != null) {
            source = Gson().fromJson(sourceJson, Source::class.java);
            bind(getSourceFromDataBase());
        }

        sourceViewModel.showChartIcon = true
        binding?.onClick = onItemClickListener;
        binding?.viewModel = sourceViewModel;

        chartState();
    }

    private fun changeSummaryChartsState(source: Source?) {
        val symbol = source?.symbol?.toUpperCase();
        val symbols = user?.symbols ?: HashMap();
        val tempChart: HashMap<String, Boolean> = symbols;
        val isChartExist = symbols.keys.indexOfFirst { it == symbol } != -1

        if(symbol == serverChart) {
            Toast.makeText(context, R.string.server_chart_limit, Toast.LENGTH_SHORT).show();
            return;
        }

        if (isChartExist) {
            sourceViewModel.chart = false;
            source?.chart = false;
            val chartKey = tempChart.keys.find { it.toUpperCase() == symbol?.toUpperCase() };
            tempChart.remove(chartKey);
            user?.symbols = tempChart;
        } else if (symbol != null && tempChart.size < Constant.Chart.LIMIT) {
            sourceViewModel.chart = true;

            source.chart = true;
            val crypto = source.type == "crypto";

            tempChart[symbol] = crypto;
            user?.symbols = tempChart;
        } else {
            Toast.makeText(context, R.string.chart_limit, Toast.LENGTH_SHORT).show();
        }
    }

    private fun chartState() {
        val charts = user?.symbols;
        val chartEnabled = charts?.keys?.indexOf(source.symbol);
        sourceViewModel.chart = chartEnabled != -1 && chartEnabled != null || source.symbol == serverChart;
    }

    fun setOnDialogDismissListener(listener: (data: Source?) -> Unit) {
        this.mListener = listener;
    }

    private fun bind(source: Source?) {
        val mySource = if (source != null) {
            source.image = this.source.image;
            source
        } else {
            this.source;
        }
        sourceViewModel.source = mySource;
    }

    private fun getSourceFromDataBase(): Source? {
        val symbol = source.symbol;
        val sources = sourceRepository.findBySymbol(symbol);

        return if (sources.isNotEmpty()) sources[0] else null;
    }
}