package ir.capapp.mobile.fragment

import android.animation.Animator
import android.animation.ValueAnimator
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.os.CountDownTimer
import android.view.*
import android.widget.*
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityOptionsCompat
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.cooltechworks.views.shimmer.ShimmerRecyclerView
import com.google.android.material.appbar.AppBarLayout
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.card.MaterialCardView
import com.google.gson.Gson
import ir.capapp.mobile.R
import ir.capapp.mobile.activity.MainActivity
import ir.capapp.mobile.activity.SearchActivity
import ir.capapp.mobile.adapter.SourceListAdapter
import ir.capapp.mobile.api.utils.AppClient
import ir.capapp.mobile.common.*
import ir.capapp.mobile.models.PriceChange
import ir.capapp.mobile.models.Source
import ir.capapp.mobile.repository.SourceRepository
import java.util.*
import kotlin.collections.ArrayList

class SourcesFragment : Fragment() {
    private lateinit var recyclerView: ShimmerRecyclerView;
    private lateinit var scrollView: NestedScrollView;
    private lateinit var search: EditText;
    private lateinit var refreshLayout: SwipeRefreshLayout;
    private lateinit var newsContainer: MaterialCardView;
    private lateinit var newsImage: ImageView;

    private var expandNews: Boolean = true;
    private var adapter: SourceListAdapter? = null;
    private var canMoveItems: Boolean = true;
    private var list: ArrayList<Source?> = ArrayList();
    private var temp: ArrayList<Source?> = ArrayList();
    private var prices: ArrayList<PriceChange> = ArrayList();
    private val sourceRepository = SourceRepository();
    private var charts: HashMap<String, Boolean>? = HashMap();
    private var serverChart: String = ServerSetting.getInstance().fallbackSourceChart;
    private var setting: Setting? = null;
    private var sourceButtonAction: SourceButtonsHandler? = null;

    private val onEditTextClickList = View.OnClickListener {
        val intent = Intent(context, SearchActivity::class.java);
        intent.putExtra("sources", Gson().toJson(temp));
        intent.putExtra("prices", Gson().toJson(prices));
        val options = ActivityOptionsCompat.makeSceneTransitionAnimation(
                context as Activity,
                it,
                "SEARCH"
        );
        context?.startActivity(intent, options.toBundle());
    }

    private var itemTouchHelperCallBack = object : ItemTouchHelper.SimpleCallback(ItemTouchHelper.UP or ItemTouchHelper.DOWN, 0) {

        var dragFrom: Int = -1;
        var dragTo: Int = -1;

        override fun onMove(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder, target: RecyclerView.ViewHolder): Boolean {
            return try {

                val firstPosition = viewHolder.adapterPosition;
                val secondPosition = target.adapterPosition;

                if (dragFrom == -1)
                    dragFrom = firstPosition;
                dragTo = secondPosition;
                adapter?.notifyItemMoved(firstPosition, secondPosition);
                Collections.swap(list, firstPosition, secondPosition);
                true;
            } catch (e: java.lang.Exception) {
                false;
            }
        }

        override fun getMovementFlags(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder): Int {
            return if (canMoveItems) {
                ItemTouchHelper.Callback.makeMovementFlags(ItemTouchHelper.UP or ItemTouchHelper.DOWN, 0);
            } else {
                0;
            }
        }

        override fun isLongPressDragEnabled(): Boolean {
            return true;
        }

        private fun drop(firstPosition: Int, secondPosition: Int) {
            val order = HashMap<String, Int>();
            for (i in 0 until list.size) {
                val item = list[i];
                if (item != null)
                    order[item.symbol] = i;
            }
            setting?.order = order;
        }

        override fun clearView(recyclerView: RecyclerView, viewHolder: RecyclerView.ViewHolder) {
            super.clearView(recyclerView, viewHolder);

            if (dragFrom != -1 && dragTo != -1 && dragFrom != dragTo)
                drop(dragFrom, dragTo);

            dragFrom = -1;
            dragTo = dragFrom;
        }

        override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        }
    }

    private val onItemClickList = { view: View, position: Int ->
        try {
            val id = view.id;

            canMoveItems = adapter?.expandedItem != true;

            if (sourceButtonAction != null) {
                when (id) {
                    R.id.item_buttons_source_detail -> {
                        sourceButtonAction?.detail(position);
                    }
                    R.id.item_buttons_source_source_bottom_sheet -> {
                        sourceButtonAction?.bottomSheet(position, adapter, childFragmentManager);
                    }
                    R.id.item_buttons_notification -> {
                        sourceButtonAction?.notificationDialog(position) {
                            recyclerView.showShimmerAdapter();
                            newRequest();
                        };
                    }
                }
            }
        } catch (ignored: Exception) {
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        val v: View = inflater.inflate(R.layout.fragment_source, container, false);
        recyclerView = v.findViewById(R.id.source_list);
        scrollView = v.findViewById(R.id.source_nested_scroll_view);
        search = v.findViewById(R.id.source_search);
        refreshLayout = v.findViewById(R.id.source_refresh);
        newsContainer = v.findViewById(R.id.source_news_main_container);
        newsImage = v.findViewById(R.id.source_news_image);

        var newsContainerHeight = 0;
        var newsImageHeight = 0;

        object : CountDownTimer(100, 100) {
            override fun onFinish() {
                newsContainerHeight = newsContainer.height;
                newsImageHeight = newsImage.height;
            }

            override fun onTick(p0: Long) {

            }
        }.start()

        val appBar: AppBarLayout? = activity?.findViewById(R.id.main_app_bar);
        search.setOnClickListener(onEditTextClickList);

        newsContainer.setOnClickListener {
            if (expandNews) {
                val containerLayoutParams = newsContainer.layoutParams;
                val imageViewLayoutParams = newsImage.layoutParams;

                val toolbarHeight = activity?.findViewById<Toolbar>(R.id.toolbar)?.height
                        ?: 0
                val bottomNavigationHeight = activity?.findViewById<BottomNavigationView>(R.id.main_navigation)?.height
                        ?: 0
                val screenHeight = getScreenHeight(activity);

//                newsImage.animate().alpha(1f).scaleX(0.95f).scaleY(0.95f).duration = 250;
                newsImage.animate().alpha(1f).duration = 250;
                expandableAnimation(newsContainer, containerLayoutParams, newsContainerHeight, screenHeight - bottomNavigationHeight - toolbarHeight - 120, 350)
                expandableAnimation(newsImage, imageViewLayoutParams, newsImageHeight, newsImageHeight + 180, 250);

                disableScroll(scrollView, true);
            } else {
                val containerHeight = newsContainer.height;
                val imageHeight = newsImage.height

//                newsImage.animate().alpha(0.4f).scaleX(1f).scaleY(1f).duration = 250;
                newsImage.animate().alpha(0.4f).duration = 250;
                val containerLayoutParams = newsContainer.layoutParams;
                val imageViewLayoutParams = newsImage.layoutParams;

                expandableAnimation(newsContainer, containerLayoutParams, containerHeight, newsContainerHeight, 350) { start ->
                    if (!start) {

                    }
                };
                expandableAnimation(newsImage, imageViewLayoutParams, imageHeight, newsImageHeight, 250);

                disableScroll(scrollView, false);
            }

            expandNews = !expandNews;
        }

        refreshLayout.setOnRefreshListener {
            refreshLayout.isRefreshing = false;
            AppClient(context).clearRequestQueue();
            newRequest();
        }
        scrollView.bouncyOverScroll();
        scrollView.setOnScrollChangeListener(NestedScrollView.OnScrollChangeListener { view, _, _, _, _ ->
            if (view?.canScrollVertically(-1) == true) {
                search.clearFocus();
                hideSoftInput(context, activity?.window?.decorView);
                appBar?.elevation = 10f;
            } else {
                appBar?.elevation = 0f;
            }
        });

        setting = Setting(context);
        charts = UserManager(context).symbols;

        initList();
        newRequest();

        return v;
    }

    private fun expandableAnimation(view: View, layoutPrams: ViewGroup.LayoutParams, from: Int, to: Int, duration: Long, listener: ((Boolean) -> Unit)? = null) {
        val vaHeight = ValueAnimator.ofInt(from, to);
        vaHeight.duration = duration;
        vaHeight.addUpdateListener {
            layoutPrams.height = it.animatedValue as Int;
            view.requestLayout();
        }
        vaHeight.start();

        vaHeight.addListener(object : Animator.AnimatorListener {
            override fun onAnimationEnd(p0: Animator?) {
                listener?.invoke(false);
            }

            override fun onAnimationCancel(p0: Animator?) {

            }

            override fun onAnimationStart(p0: Animator?) {
                listener?.invoke(true);
            }

            override fun onAnimationRepeat(p0: Animator?) {

            }
        });
    }

    private fun getSources() {
        (context as MainActivity).getSource { sources, specials, prices, references, error ->
            if (sources != null && prices != null) {
//                writeCacheData(sources, Constant.Action.SOURCE);
                this.prices.clear();
                this.prices.addAll(prices);
                margeSources(sources);
                notifyList()
            } else {
                //error here
            }
        }
    }

//    private fun notificationDialog(source: Source?) {
//        val dialog = AlertDialog.Builder(context).create();
//        val view = LayoutInflater.from(context).inflate(R.layout.dialog_change_price, null, false);
//        dialog.setView(view);
//
//        val button: MaterialButton = view.findViewById(R.id.dialog_change_price_submit);
//        val loading: ProgressBar = view.findViewById(R.id.dialog_change_price_loading);
//        val deleteButton: MaterialButton = view.findViewById(R.id.dialog_change_price_delete);
//        val value: TextView = view.findViewById(R.id.dialog_change_price_value);
//
//        val indexOfPrice = prices.indexOfFirst { it.symbol == source?.symbol };
//        val priceChange: PriceChange = if (indexOfPrice != -1) {
//            deleteButton.visibility = View.VISIBLE;
//            val p = PriceChange();
//            p.symbol = source?.symbol;
//            p.unit = source?.unit;
//            value.text = prices[indexOfPrice].threshold.toString();
//            p;
//        } else {
//            val p = PriceChange();
//            p.symbol = source?.symbol;
//            p.unit = source?.unit;
//            p;
//        }
//
//        deleteButton.setOnClickListener {
//            deleteButton.visibility = View.GONE;
//            button.visibility = View.GONE;
//            loading.visibility = View.VISIBLE;
//
//            API.Subscription.Notification.priceChangeDelete(context, source?.symbol) { error ->
//                if (error != null) {
//                    deleteButton.visibility = View.VISIBLE;
//                    button.visibility = View.VISIBLE;
//                    loading.visibility = View.GONE;
//
//                    Toast.makeText(context, "خطای غیر منتظره دوباره تلاش کنید.", Toast.LENGTH_SHORT).show();
//                } else {
//                    recyclerView.showShimmerAdapter();
//                    newRequest();
//                    dialog.dismiss();
//                }
//            }
//        }
//
//        button.setOnClickListener {
//            try {
//                deleteButton.visibility = View.GONE;
//                button.visibility = View.GONE;
//                loading.visibility = View.VISIBLE;
//
//                priceChange.threshold = value.text.toString().toFloat();
//
//                API.Subscription.Notification.priceChange(context, priceChange) { error ->
//                    if (error == null) {
//                        recyclerView.showShimmerAdapter();
//                        newRequest();
//                        dialog.dismiss();
//                    } else {
//                        deleteButton.visibility = View.VISIBLE;
//                        button.visibility = View.VISIBLE;
//                        loading.visibility = View.GONE;
//
//                        dialog.dismiss();
//                    }
//                }
//            } catch (e: java.lang.Exception) {
//                e.printStackTrace();
//                button.visibility = View.VISIBLE;
//                loading.visibility = View.GONE;
//            }
//        }
//
//        dialog?.window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_VISIBLE);
//        dialog.show();
//        AppClient.getInstance(context).clearRequestQueue();
//    }

    private fun newRequest() {
        list.clear();
        temp.clear();
        adapter?.resetAdapter();
        adapter?.notifyDataSetChanged();
        recyclerView.showShimmerAdapter();
        getSources();
//        val offlineSources = readCacheData(Constant.Action.SOURCE);
//        if(offlineSources != null) {
//            val sources: ArrayList<Source> = Gson().fromJson(offlineSources, object : TypeToken<ArrayList<Source>>(){}.type);
//            margeSources(sources);
//            notifyList();
//        } else {
//        }
    }

//    private fun writeCacheData(data: ArrayList<Source>, key: String) {
//        val sourcesJson = Gson().toJson(data);
//        val pref = SharedPreferencesHelper(context);
//
//        val sourceDataJson = JSONObject();
//        sourceDataJson.put("expirationDate", Constant.Cache.expiresOn);
//        sourceDataJson.put("data", sourcesJson);
//
//        pref.save(key, sourceDataJson.toString());
//    }

//    private fun readCacheData(key: String): String? {
//        val pref = SharedPreferencesHelper(context);
//        val sources = pref.getString(key, "");
//
//        if (sources != "") {
//            val sourcesJson = JSONObject(sources);
//            if (sourcesJson.getLong("expirationDate") > now)
//                return sourcesJson.getString("data");
//        }
//
//        return null;
//    }

//    private fun showBottomSheet(item: Source) {
//        val bundle = Bundle();
//        bundle.putString(Constant.Action.SOURCE, Gson().toJson(item));
//        val sourceBottomSheet = SourceBottomSheet();
//        sourceBottomSheet.setOnDialogDismissListener { s ->
//            updateItem(s);
//        }
//        sourceBottomSheet.arguments = bundle;
//        sourceBottomSheet.show(childFragmentManager, "tag");
//    }

//    private fun updateItem(s: Source?) {
//        try {
//            val index = list.indexOfFirst { it?.symbol == s?.symbol };
//            val source = list[index];
//            source?.favorite = s?.favorite ?: false;
//            source?.chart = s?.chart ?: false;
//            source?.capital = s?.capital;
//            adapter?.notifyDataSetChanged();
//        } catch (e: Exception) {
//            e.printStackTrace();
//        }
//    }

    private fun margeSources(sources: ArrayList<Source>) {
        val mySources = ArrayList(sourceRepository.findAll());
        for (source in sources) {
            val indexOfPriceChange = prices.indexOfFirst { it.symbol == source.symbol }
            if (indexOfPriceChange != -1) {
                source.favorite = true;
            }

            for (mySource in mySources) {
                if (mySource.symbol == source.symbol) {
                    source.capital = mySource.capital;
                }
            }

            val chartEnabled: Int? = charts?.keys?.indexOfFirst { it.toUpperCase() == source.symbol.toUpperCase() };
            source.chart = chartEnabled != -1 && chartEnabled != null || source.symbol == serverChart;

            if (source.enabled || !source.message.isEmpty())
                list.add(source);
        }

        temp.addAll(list);
        sourceButtonAction = SourceButtonsHandler(context, list, prices)

        try {
            val order = setting!!.order;
            for (o in order.keys) {
                val index = list.indexOfFirst { it?.symbol == o };
                val orderIndex: Int = order[o] ?: 0;
                if (index != -1 && orderIndex < list.size) {
                    Collections.swap(list, index, orderIndex);
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }

        adapter?.notifyDataSetChanged();
    }

    private fun notifyList() {
        recyclerView.hideShimmerAdapter();
        adapter?.notifyDataSetChanged();
    }

    private fun initList() {
        adapter = SourceListAdapter(context, list);
        adapter?.onItemClickListener(onItemClickList);
        recyclerView.isNestedScrollingEnabled = false;
        recyclerView.layoutManager = LinearLayoutManager(context, RecyclerView.VERTICAL, false);
        recyclerView.itemDecoration(context, R.drawable.inset_right_divider);
        recyclerView.adapter = adapter;
        recyclerView.showShimmerAdapter();

        val ith = ItemTouchHelper(itemTouchHelperCallBack);
        ith.attachToRecyclerView(recyclerView);
    }
}