package ir.capapp.mobile.repository

import ir.capapp.mobile.application.App
import ir.capapp.mobile.models.DaoSession

abstract class MasterDaoSession {
    protected var daoSession: DaoSession? = null;

    init {
        this.daoSession = App.daoSession;
    }
}