package ir.capapp.mobile.repository

import ir.capapp.mobile.models.Source
import ir.capapp.mobile.models.SourceDao

class SourceRepository : MasterDaoSession(), BaseRepository<Source> {

    private val sourceDao = daoSession!!.sourceDao;

    override fun findById(id: Long): Source {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    fun findBySymbol(symbol: String): List<Source> {
        return sourceDao
                .queryBuilder()
                .where(SourceDao.Properties.Symbol.eq(symbol))
                .list();
    }

    override fun findAll(): List<Source> {
        return sourceDao
                .queryBuilder()
                .list();
    }

    override fun update(`object`: Source?): Long {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun insert(`object`: Source?): Long {
        return sourceDao
                .insert(`object`);
    }

    fun insertOrReplace(`object`: Source?): Long {
        return sourceDao
                .insertOrReplace(`object`);

    }

    override fun delete(`object`: Source?) {
        sourceDao.queryBuilder()
                .where(SourceDao.Properties.Symbol.eq(`object`?.symbol))
                .buildDelete()
                .executeDeleteWithoutDetachingEntities();
    }
}