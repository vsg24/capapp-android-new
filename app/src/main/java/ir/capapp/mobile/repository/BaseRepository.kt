package ir.capapp.mobile.repository

interface BaseRepository<T> {
    fun findById(id: Long): T;
    fun findAll(): List<T>;
    fun update(`object`: T?): Long;
    fun insert(`object`: T?): Long;
    fun delete(`object`: T?);
}