package ir.capapp.mobile.models;

public class FreeTrial {
    public Boolean enabled;
    public String message;
    public long duration;
    public String sku;
}