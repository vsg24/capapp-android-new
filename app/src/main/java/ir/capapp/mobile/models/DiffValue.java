package ir.capapp.mobile.models;

public class DiffValue {
    public Float value;
    private Float percentage;

    public Float getPercentage() {
        if(percentage != null)
        return (float) Math.round(percentage * 100) / 100;
        else
            return 0f;
    }

    public void setPercentage(Float percentage) {
        this.percentage = percentage;
    }
}
