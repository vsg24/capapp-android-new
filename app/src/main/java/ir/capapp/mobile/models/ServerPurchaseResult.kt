package ir.capapp.mobile.models

public class ServerPurchaseResult {
    var instanceId: String? = null;
    var initiationTimestampSec: Long = 0;
    var validUntilTimestampSec: Long = 0;
}