package ir.capapp.mobile.models;

import java.util.HashMap;

public class Special {
    public String title;
    public String image;
    public String unit;
    public HashMap<String, Double> values;
}
