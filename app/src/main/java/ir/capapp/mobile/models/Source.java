package ir.capapp.mobile.models;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.Index;

import kotlin.jvm.Transient;
import org.greenrobot.greendao.annotation.Generated;

@Entity
public class Source {

    public Source() {

    }

    @Generated(hash = 596064366)
    public Source(Long sourceId, String title, String symbol, String type,
            String unit, Double capital, boolean favorite) {
        this.sourceId = sourceId;
        this.title = title;
        this.symbol = symbol;
        this.type = type;
        this.unit = unit;
        this.capital = capital;
        this.favorite = favorite;
    }

    public Long getSourceId() {
        return this.sourceId;
    }

    public void setSourceId(Long sourceId) {
        this.sourceId = sourceId;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSymbol() {
        return this.symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getType() {
        return this.type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUnit() {
        return this.unit;
    }

    public void setUnit(String unit) {
        this.unit = unit;
    }

    public Double getCapital() {
        return this.capital;
    }

    public void setCapital(Double capital) {
        this.capital = capital;
    }

    public boolean getFavorite() {
        return this.favorite;
    }

    public void setFavorite(boolean favorite) {
        this.favorite = favorite;
    }

    @Id(autoincrement = true)
    @Index
    public Long sourceId;

    @Index
    public String title;

    @Index
    public String symbol;

    public String type;

    public String unit;

    @Transient
    public Double value;

    @Transient
    public Boolean chart;

    @Transient
    public int trending;

    @Transient
    public Double tempValue;

    public Double capital;

    public boolean favorite;

    @Transient
    public CreatedAt entityCreatedAt;

    @Transient
    public CreatedAt valueCreatedAt;

    @Transient
    public Double averageValue;

    @Transient
    public String message;

    @Transient
    public String image;

    @Transient
    public String bgImage;

    @Transient
    public Boolean enabled;

    @Transient
    public Diff diffs;

}