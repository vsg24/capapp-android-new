package ir.capapp.mobile.models;

public class PriceChange {

    public String symbol;
    public Double threshold;
    public String unit;
}
