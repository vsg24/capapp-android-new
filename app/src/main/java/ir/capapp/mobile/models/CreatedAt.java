package ir.capapp.mobile.models;

public class CreatedAt {

    public StandardDate standard;
    public String persianFormatted;
    public Long timestamp;
}
