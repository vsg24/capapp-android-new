package ir.capapp.mobile.models

public class InAppPurchasableProductPurchaseResult {
    var purchaseToken: String? = null;
    var orderId: String? = null;
    var productId: String? = null;
}